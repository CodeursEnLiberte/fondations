# Assemblée générale ordinaire Codeurs en Liberté

3 mars 2020 pour le bilan 2019

**Présents** :

* Antoine
* Thimy
* Khéops
* Pierre
* Tristram
* Vincent
* Thibaut
* Nicolas


**Absents :**
* Francis
* Étienne


**Observateurs :**
* Nelson


**Procurations :**
* Aucune procuration n'est déposée.

### Présentation du rapport de gestion

### Validation des comptes

Résolution soumise au vote :

* Pour 👍 : 8
* Contre 👎 : 0
* Abstention 😶 : 0

### Affectation du résultat

Le résultat est affecté à hauteur de 15% en réserve (on a augmenté le capital, la réserve est donc à nouveau sous les 100% du capital) et 85% de report à nouveau (pas de dividendes).

Résolution soumise au vote :

* Pour 👍 : 8
* Contre 👎 : 0
* Abstention 😶 : 0

### Quitus de gestion pour le président

Résolution soumise au vote :

* Pour 👍 : 8
* Contre 👎 : 0
* Abstention 😶 : 0

### Élection à la présidence

Candidat·e·s :
* Thimy Kieu

Thimy Kieu est élue présidente à l’unanimité. Elle prend la succession de Pierre de La Morinerie.

La rémunération pour l’activité de présidence est fixée à 0 €.

### Autres résolutions

Participation à un appel à projet européen en lien avec mes-aides.
Vote sur la résolution, sous réserve que la trésorerie soit disponible :

* Pour 👍 : 8
* Contre 👎 : 0
* Abstention 😶 : 0

## Fin de séance

L’ensemble de l’ordre du jour ayant été épuisé, l’assemblée générale est levée à 22 h 02.
