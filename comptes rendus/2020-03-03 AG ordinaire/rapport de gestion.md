# Rapport de gestion

Situation et activité de la société durant l’exercice 2019.

## Bilan financier

La liasse fiscale fait foi et c’est elle qui sera soumise à votre approbation.

2019 a la première pleine avec le contrat cadre avec beta.gouv.fr qui a structuré l’année.

* Chiffres d’affaires : 1 775 288 €
* Sous-traitance : 858 832 €, soit 48%
* Chiffre d’affaires pas lié à beta.gouv.fr : 275 761 € soit 16%
* Salaires
  * Total : 854 776 €
  * Salaire immédiat : 490 180 €
  * Salaire socialisé : 358 061 €
* Taxes et impôts (taxe d'apprentissage, etc.) : 6 535 €
* Résultat net : 11 143 €

## Actions

Nous avons investi une semaine en fonds propres pour une semaine de **Contribution au Libre**, en finançant en plus de nos salaires les frais d’invités. L’expérience était intéressante, mais nécessite encore de trouver le moyen de passer à l’échelle en ambition et en récurrence.

Une présentation sur notre mode de fonctionnement dans le cadre de **Pas Sage en Seine** a eu un bon écho. Un stagiaire nous a rejoint suite à cette présentation ; nous avons aussi été invités à refaire cette présentation à l’école 42.

Notre **séminaire** en octobre a été à nouveau l’occasion de travailler sur des projets communs pendant une semaine. À cette occasion, nous avons publié **code-code.coop**, une première version d’un guide de création de coopératives dans la tech, et démarré **chuis.la**, un outil de localisation respectueux de la vie privée.

Nous avons également été impliqués dans la formation au travers de l’école **Ada School**, qui s’efforce entre autres de ré-équilibrer le faible nombre de femmes dans l’informatique.

## Membres de Codeurs en Liberté

Nous sommes restés stables à dix membres. L’année a été marquée par le passage de notre premier stagiaire. Cela s’était très bien passé et nous a encouragé à vouloir nous impliquer un peu plus dans l’intégration de profils apprenants.
L’année a aussi été marqué par une décision difficile en ce qui concerne le recrutement et a marqué de fait une envie d’être très prudents afin de préserver nos modes de fonctionnement.

## Évolution du marché DINUM (La Zone)

Une incertitude continue à planer quant à la nature du travail à l’avenir. Certains membres de Codeurs en Liberté souhaitent prendre un peu de distance afin de diversifier les sujets.

Le coût de gestion reste très faible et n’est pas un souci particulier.

Il y a eu un moment difficile avec une startup d’état dont la paiement a été fortement retardé et dont les intervenants ont travaillé sans bon de commande, contre nos instructions explicites.

Nous pensons cependant être capables de porter un plus grand nombre de projets grâce à notre bilan financier 2019 qui est très solide et permettra, entre autres, de demander un découvert plus important auprès de la banque, sans compter la marge qui alimente progressivement notre besoin en fond de roulement.
L’ensemble des 35 000€ de compte courant d’associés qui ont alimenté notre besoin de fond de roulement ont été remboursés.

## Banques

La migration de la Bred au Crédit Mutuel n’est toujours pas finie. La DINUM continue à payer les bons de commande sur la Bred malgré nos échanges affirmant le contraire.

De l’autre côté, les multiples facteurs d’authentification du Crédit Mutuel agacent.

## Évènements importants survenus depuis la clôture
Néant

## Activité de recherche et développement
Néant

## Prise de participation
Néant

## Filiales et participations
Néant

## Dépenses non déductibles fiscalement
Néant

## Conventions réglementées visées à l'article L. 223-19 du Code de commerce
Aucune convention relevant de l'article L. 223-19 du Code de commerce n’a été portée à la connaissance du gérant.

## Résolutions particulières

### Choix d’un nouveau président
Mon mandat de président finit avec cet exercice. Conformément au mécanisme de présidence tournante de Codeurs en Liberté, je souhaite qu’un autre membre me succède à ce poste.

## Conclusion
Au revouâr.
