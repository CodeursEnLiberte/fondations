# Bilan septembre

Bonjour,

Je vais tenter de faire un bilan mensuel, pour récapituler un peu ce qui se passe. Il ne faut pas hésiter à poser des questions. Le but c’est d’arriver au minimun de surprises et que des futurs arrivants puissent refaire un peu l’historique.

## Bilan septembre

C’est donc le mois où ça commence à gentillement à se mettre en place. La première facturation a été faite, le gros des obligations sociales sont réglées.

## Kheops

Est allé faire un tour au Canada et a mis en place le serveur. Il y a un owncloud pour stocker tous les documents. Login et mots de passe sont transmis sur demande à condition d’avoir une adresse mail avec une clef GPG.

C’est aussi le premier à ramener de l’argent 💰

## Tristram

### Côté administration

- Ouverture du compte en banque
- Médecine du travail
- Apprentissage de la compta
- Paye
- Retraites complémentaires obligatoires
- Urssaf
- Essaye de réduire les cotisation sociale (retraite et accident travail), car on avait été classé formation et non pas développement. Succès pour les retraites, en attente de réponse pour l’accident du travial
Une comptabilité simplifiée peut être trouvée dans Administration/Ressources humaines/État des comptes séparés.odt

### Côté *travail*

Un tour du côté de Bruxelles pour State of the Map, la conférence internationnale sur OpenStreetMap.

Travail sur un calculateur d’itinéraires indoor. Je referai un ticket ici pour détailler les raisons.

Beaucoup de pot/cocktail/events juste pour se montrer, poire et manger à l’œil, discuter un peu de Codeurs en Liberté. Le concept semble plaire et s’explique assez bien

## Pierre

### Côté travail

J'ai posé ma démission de Captain Train début septembre. Avec les trois mois de préavis, je serai libre au plus tard début décembre. Je pars ensuite au Mexique jusqu'à mi-janvier – et ensuite je pourrais faire plus de choses avec les Codeurs en Liberté, me chercher un premier projet, tout ça :)
