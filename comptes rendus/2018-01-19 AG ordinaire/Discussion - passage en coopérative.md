# Décisions

- on veut rencontrer d'autres structures similaires d'abord pour se faire une
  meilleure idée, comme [ut7](https://ut7.fr) ;
- on veut étudier plus clairement le bénéfice sociétal de l'URSCOP car il semble
  qu'ils ne nous apporteront pas grand chose à nous directement.

# Actions

- Tristram s'engage à lire le rapport d'activité d'URSCOP ;
- Nicolas, Vincent et Tristram motivés pour aller voir ut7.

# Éléments de discussion

Il y a plusieurs types de coopératives. On peut aussi simplement s'en
revendiquer, sans statut administratif spécifique. Dans tous les cas, si on le
fait c'est surtout par revendication car nos pratiques correspondent déjà à une
coopérative.

Le statut légal donne des avantages. La participation est déduite des bénéfices
donc pas d'impôts dessus ; pas de cotisation foncière des entreprises (CFE) ni
de cotisation sur la valeur ajoutée des entreprises (CVAE).

Si on rentre dans l'URSCOP, un pourcentage de notre CA va à eux. Sinon, la
contrepartie c'est plus de paperasse un peu incertaine et laborieuse à faire
chaque année.

Mention du statut de CAE, qui permet d'avoir des contrats de travail qui ne
respectent pas toutes les obligations légales classiques. Pour faire du portage.
Souvent considéré comme une structure transitoire qui « mène à l'emploi ». Pas
vraiment pour nous.

Au final :

- point plutôt contre :
    - adhérer à l'URSCOP ou passer en SCOP sans eux représente un effort
      supplémenaire (paperasse, temps passé) pour pas énormément de contrepartie
- plutôt pour :
    - le statut officiel de SCOP peut consolider l'aspect revendicatif
    - l'URSCOP a un côté militant/lobbying et des activités (comme
      l'accompagnement d'autres structures) qu'on peut vouloir contribuer à
      financer en remplaçant les impôts classiques par le pourcentage de CA payé
      à l'URSCOP, même si ça ne nous bénéficie pas directement

