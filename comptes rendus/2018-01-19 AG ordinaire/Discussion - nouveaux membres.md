# Adhésion de nouveaux membres / Comment grossir

## Idées en vrac

* Comment dupliquer le principe?
* Si on grandit, ça amène de la bureaucratisation
 * Un comptable atitré, par exemple
* On ne peut pas simplement dire à des personnes intéressées de créer leur structure similaire
* Alternativement, si on faisait un “incubateur de coopérative”, une  hiérarchie se mettrait nécessairement en placeL
* Tout le monde n’est pas forcément intéressé par les formalités pour faire une entreprise
* Mais c’est _libérant_ de comprendre les processus
* On documente nos actions pour aider les structures similaires
 * D’un autre côté, c’est compliqué à écrire et à lire, et ça n’apporte pas beaucoup par rapport aux organismes officiels. Mieux vaut aider des gens directement.
* “Mitose”
 * Grossir jusqu’à la limite haute pour notre fonctionnement (environ 15 persones), puis diviser en deux.
 * C’est compliqué. A priori non. On verra le cas échéant quand on sera nombreux, si des équipes ou des projets se forment natuellement.
* “Loges”
 * des sous-groupes qui font leur vie sous la même structure légale
* Si un jour on a des locaux, ça pourrait aider à intégrer les nouveaux de les faire venir en coworking.

## Principes

* Taille limite raisonnable: environ 10 personnes
* Pour l’instant, pas un problème. On verra dans un an quand on aura réellement besoin.

## Processus

* Définir un référent pour chaque candidat (ce qu’on fait déjà)
* Publier un “guide pour nous rejoindre”.

---

# Actions

* En ce qui concerne la taille maximum, la division en plusieurs entités… on verra dans un an combien on est dans la structure.

* Documenter publiquement le process d’accueil (@nicolas)
 - deux filières: cooptation et spontané
 - premiere discussion sur notre fonctionnement et les structures similaires alternatives
 - accès à nos chats et outils pour qu’on se découvre, pour les gens qui n’ont pas déjà travaillé avec l’un de nous. Pendant par exemple 2 semaines.
 - rencontre de tout le monde en réel ou en visio, en moins de deux semaines à partir du moment où le nouveau a pris décision de nous rejoindre

Brouillon:
```
Pourquoi et comment nous rejoindre ?
1. Lisez notre doc
2. Regardez les alternatives. On peut aussi vous filer des astuces pour monter votre structure
3. Rencontrez-nous tous.
 * chacun en “entretien”
 * sur mattermost
 * en coworking, ou plusieurs jours dans un weekend
4. Une fois que la personne décide de vouloir nous rejoindre, on se décide rapidement.
* On peut aussi imaginer des _copains_ participant à toutes les discussions, voire même sociétaires de la coop, sans être salariés.
```

## Contacts en cours

* Francis: 🎉
* Donner accès à mattermost aux personnes en contact, voir quels sont leurs calendriers, les rencontrer
* Prendre des nouvelles des personnes contactées précédemment.
