# Décision

- pas à l'ordre du jour de trouver des locaux à nous ;
- c'est plus important de trouver des copains, dans l'immédiat ;
- garder les yeux et les oreilles ouverts.

# Éléments de discussions

Pas assez d'activité pour porter quelque chose nous-même. Faudrait partager. On
aimerait une cuisine, principalement.

Question à se poser de ce qu'on veut faire d'un local collectif. Potentiellement
chouette si on peut aménager quelque chose ensemble. Micro conférences,
évènements, etc.

Ne peut pas non plus être porté par nous car on est pas tous à Paris, pas tous
toujours disponibles. Pourrait le faire en trouvant une autre structure.

Implique la nécessité de se mettre en réseau avec d'autres structures.

Ça pourrait être étendu vers projet politique, corporation, etc. 
