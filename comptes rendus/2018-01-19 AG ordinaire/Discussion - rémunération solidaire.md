Voir le ticket [29](https://gitlab.com/CodeursEnLiberte/fondations/issues/29) sur Gitlab pour les discussions préalables.

# Réunion du fonds de gestion et du pot à cookies

Acté : on réunit les deux et on flèche le budget général vers les communs.

Note de discussion : l'avantage de séparer était qu'on savait ce qu'on avait
« le droit » de piocher dans le pot à cookies pour ne pas ponctionner la
gestion. En réalité, on a de toute façon largement assez.

# Rémunération solidaire

But : arriver à une formule de calcul pour le salaire de chacun selon le CA
généré.

## Décisions

- taux de -5% sur la tranche de CA de 0-30k€ annuels ;
- taux de 15% sur tout ce qui est au-delà de 30k€ ;
- taux appliqué mensuellement au CA apporté par la personne, en projetant sur
  l'année et en rééquilibrant au fur et à mesure pour que le taux annuel soit
  correct ;
- dans le cas de petites rentrées d'argent ponctuelles insufisantes pour payer
  un salaire, on garde un taux fixe de 10%.

## Actions

- coder la nouvelle ardoise : Tristram, Thi My (échance : fin février) ;
- mettre à jour la documentation partout : Nicolas (échéance : fin février).

## Éléments de discussion

Plusieurs enjeux un peu différents mentionnés qui touchent à cette question :

- les personnes bloquées dans le statut précaire d'auto-entrepreneur du fait
  d'un chiffre d'affaire assez faible : on aimerait rapproches la limite du
  statut d'AE pour pouvoir être salarié chez CeL ;
- les écarts de salaire (entre le plus élevé et le plus bas) qu'on pourrait
  vouloir réduire (problématique d'avoir des salaires plus « justes ») ;
- l'envie d'encourager à contribuer aux communs via le pot à cookies ;
- prendre en compte les besoins spécifiques légitimes individuels (exemple :
  enfants à charge) ;
- laisser au mieux à chacun·e la possibilité de lisser son salaire comme ille
  l'entend.

Sur la réduction des écarts de salaires : c'est un critère de l'Économie sociale
et solidaire (ESS) mais dont l'application se justifie surtout lorsqu'il y a des
relations de subordination dans une entreprise. Chez nous il n'y en a pas et les
CA individuels sont très variables donc tenter de réduire les écarts de salaire
est à la fois moins justifié et plus compliqué à mettre en place.

Enjeux nombreux et problème pas facile, donc péférence générale pour voir les
questions les unes après les autres, et commencer par quelque chose de simple
pour le moment. Questions ouvertes pour aller plus loin mais pour l'instant on
va commencer par des palier simples comme proposé par Tristram sur le ticket.

Sur la question de pouvoir lisser son salaire d'une année à l'autre (un très
gros CA à l'année N et rien à l'année N+1), l'effet de ces paliers est
négligeable.

Autres notes et remarques en vrac :

- les missions longues qu'on a tous eu n'a pas encouragé à piocher dans le pot à
  cookies ;
- des idées de payer du temps utiliser pour s'occuper de problèmatiques interne
  (développement d'outils) mais risques liés de bureaucratisation : à rediscuter
  plus tard ;
- question de réduire la contribution au pot à cookies si une personne fait une
  mission qui contribue aux communs : on met pas de règles, on verra au cas par
  cas.


