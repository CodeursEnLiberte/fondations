# Assemblée générale ordinaire

Vendredi 19 janvier 2018

**Présents** :

* Tristram
* Vincent
* Nicolas
* KheOps
* Thimy

**Excusés** :

* Pierre 🙏

**Début de scéance** :

Description de tâches à faire durant l’AG :

* Présentation du bilan financier de l’année
* Valider les comptes
* Voter ce qu’on fait du résultat
* "Élire" un nouveau président

# Présentation du rapport de gestion

## Validation des résultats
Obligation légale, accès à tous les chiffres par tous les membres.
Voter la liasse fiscale.
Codeurs en Liberté, "ça marche". 🎉 Pas de difficultés de fonctionnement ou de choix.

💰 395000€ de chiffre d'affaires :

* 185000€ en paie directe
* 147000€ en cotisations sociales

--> forment le superbrut

* dépenses communes (15000€) : mettre la complémentaire santé dedans ? 
* frais individuels : 3600€ (pc, frais de téléphone et internet, etc.)
* 2000€ de dons

--> brut d'exploitation : 41000€, excédent de plus de 10% à cause de Vincent.

* impôts des sociétés à 15% (6000€).

Résultat net (bénéfice après impôts) : 35000€. Ne correspond pas à ce qu’il y a en banque, manque un trimestre de cotisations, un mois de TVA mais Octo doit encore 20000€.

395000€ de marchandise vendue dont 45000€ en export.

La liasse fiscale sera transmise à la banque et aux greffes.

## Affectation du résultat
Bénéfice de 35000€.
Proposition : 

* 15% en réserve (on met de côté et on n'y touche pas) obligation en coopérative et non partageable
* 85% en report à nouveau (on ne sait pas quoi en faire, on n'y touche pas mais reste en circulation)
* 0% en dividende

Somme affectée au chiffre d'affaires d'une personne.

## Perspectives 2018
Avec l’expérience, on va pouvoir être plus précis.
Gros chantier de passage en coopérative.
Que faire du chiffre d'affaires, fusion pot à cookies/frais de gestion.

## Calcul d’une prime exceptionnelle
50% du résultat (soit 17612€) affecté au calcul d’une prime exceptionnelle selon la méthode suivante :
275,19€ par salarié et par mois effectivement travaillé
Le montant sera affecté au chiffre d’affaires de chaque salarié

Une partie du bénéfice fait partie du salaire de Vincent. (12000€)
Est-ce qu’on épuise le pot à cookie ? 🍪
On affecte 17000€ répartis uniformément en fonction du nombre de mois travaillés et indépendemment du salaire.

# Résolutions soumises au vote

## Validation des comptes
Liasse fiscale : on valide tout sauf la page 6 (capital social vide).
Adopté à l’unanimité.

## Affectation du résultat
Adopté à l’unanimité.

## Quitus de gestion
Considère-t-on que la présentation a été fidèle à la réalité ?
Adopté à l’unanimité.

## Élection du président
On conserve le siège social actuel.
Tristram propose de continuer à faire la paperasse.
Le président est désigné par décision collective.
Il faut définir une durée et une rémunération.

🕴 Nicolas est désigné par les associés unanimement jusqu’à la prochaine AGO pour un salaire de 0 euro.

## Calcul d’une prime exceptionnelle
Adopté à l’unanimité.
