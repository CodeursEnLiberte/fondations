# Accord de participation

* Pas d’obligation en dessous de 50 salariés, on peut quand même.
* À déclarer formellement avant juin de l’année d’exercice

* Proposition : 50% du bénéfice, redistribuer en participation
 * au prorata des mois salariés, ou au nombre réel de jours travaillés.
* C’est ce qu’on fait pour l’exercice 2017-2018, mais sans “participation” formelle.
* Avantage fiscal à le faire formellement, assez négligeable pour l’entreprise. Pour les salariés, elle peut être versée sur le PEE, qui n’est pas soumis à l’impôt sur le revenu.
* Cela dit, plus simple à faire ainsi qu’à la fin de l’année pendant l’AG.

# Actions

* Signer la déclaration formelle, avant juin (Nicolas, Tristram)