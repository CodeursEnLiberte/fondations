# Assemblée générale ordinaire pour l’exercice 2022

Heure de début : Jeudi 4 mai 2023 à 10h15

**Présent·e·s** :

* Tristram Gräbener
* Vincent Lara
* Antoine Desbordes
* Francis Chabouis
* Nicolas Bouilleaud
* Thibaut Sailly
* Thimy Kieu
* Thomas Guillet
* Nicolas Bouilleaud

**Procurations**

* Pierre de la Morinerie à Tristram Gräbener
* Aurélien Mérel à Tristram Gräbener

**Absent·e·s**

## Résolutions

### Validations des comptes
Selon les chiffres de la liasse fiscale en annexe

#### Résultats du vote
 * Pour : 11
 * Contre : 0
 * Abstention : 0

### Affectation du résultat
Le résultat net après impôts est de 54 733 €. Voici l’affectation proposée (en accord avec les obligations de réserve légale coopérative et l’accord de participation) :

 * Dividendes : 0 €
 * Réserve coopérative : 0 €
 * Report à nouveau : 54 733 € le portant à 254 982 €

**Participation** conformément à l’accord de participation, 27 366,50 € (50% du résultat net) sont affecté à la participation. Par personne et par mois travaillé cela représente 248,79 €.

#### Résultats du vote
 * Pour : 11
 * Contre : 0
 * Abstention : 0

### Quitus de gestion du président
Nous sommes entièrement satisfait·es de sa gestion et lui donnons le quitus.

#### Résultat du vote
 * Pour : 11
 * Contre : 0
 * Abstention : 0

### Élection à la présidence

Vincent Lara, président sortant, ne souhaite pas prendre un nouveau mandat.

Thomas Guillet se présente comme unique candidat. Nous l’élisons président.

Le mandat de président ne comporte aucune indemnité lié à la fonction et l’intégralité du salaire doit provenir du travail facturé aux clients.

#### Résultats du vote
 * Pour : 11
 * Contre : 0
 * Abstention : 0


# Clôture
L’ordre du jour étant épuisé, la séance est close à 10h30.

