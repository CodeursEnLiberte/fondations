# Rapport de gestion pour l’exercice 2022

## Bilan financier
La référence est la liasse fiscale en annexe ; c’est elle qui fait foi et qui est soumise à l’approbation des sociétaires.
Nous présentons ici des chiffres simplifiés pour avoir une idée de l’activité.

 * Chiffre d’affaires : 842 895 €
 * Salaires : 504 769 €
 * Cotisations patronales : 209 635 €
 * Résultat brut : 64 210 €
 * Résultat avant impôts : 67 786 €
 * Impôts sur les bénéfices : 13 053 €
 * Résultat net : 54 733 €

Le résultat est artificiellement élevé suite à plusieurs facteurs exceptionnels :

- nous n’avons pas encore traité les factures impayées qu’il faudra probablement considérer comme des pertes,
- le contrôle urssaf a permis de constater des anomalies en notre faveur dans la déclarations des abondements,
- les ardoises individuelles se sont plutôt remplies.


## Bilan moral

### Formation et accompagnement

Nous avons accueilli trois stagiaires cette année, une a rejoint l'équipe du simulateur de 1jeune1solution.gouv.fr, une autre sur transport.data.gouv.fr et l'autre a travaillé sur cocarto.

Lors de l’assemblée générale précédente, nous nous étions engagés à faire un bilan de l’accueil en apprentissage. Ce bilan n’a pas été fait.

Des discussions informelles en séminaire ont souligné la difficulté de bien accueillir des profils avec très peu d’expérience. Un accompagnement quotidien semble indispensable, créant un conflit avec nos habitudes de travail.

Nous nous posons des questions sur comment accueillir au mieux de nouveaux stagiaires, nous pensons à tester des stages sur des plus petits périmètres fonctionnels en dehors de fonctionnalités cœur. Mais nous gardons en tête qu’il est gratifiant de voir son travail en production et de constater les effets.

On note une difficulté à accueillir des stagiaires en stage lorsqu’on est en prestation notamment car il est difficile de facturer plein pot l’encadrement d’un stagiaire.

Ce sujet en particulier fera l’objet d’une discussion au prochain séminaire.

### Diversité

Comme à chaque assemblée générale, nous constatons que nous restons significativement plus mauvais que les autres sociétés de services en informatique.

Il nous a été rapporté que notre image extérieure est celle d’un « boys club ».


### Fonctionnement courant et conditions de travail

#### Contrôle urssaf

Malheureusement, au moment de la préparation de l’assemblée générale, le contrôle urssaf n’est pas encore fini.

Il a révélé trois grand éléments :
— de nombreuses erreurs dans la déclaration de l’abondement (en notre faveur, on a trop payé l'urssaf sur l'abondement)
— une prime forfaitaire repas-transport-télétravail trop importante.
— un manque de rigueur sur les décisions d’entreprise (par exemple au sujet de la prime de partage de la valeur) qui sont prises oralement sans trace écrite

Cela repose la question si nous ne devrions pas prendre les service de comptable et mieux acter les décisions qui seraient prises par une direction. Quant aux frais de repas et transport, nous attendons le rapport de l’urssaf pour mieux comprendre ce que nous pouvons faire.

On reporte la discussion de recourir au service d’un comptable à une assemblée générale extraordinaire ultérieure.

### Retour sur les perspectives annoncées prévues pour 2022

Lors de l’assemblée générale ordinaire de 2021, nous avions émis plusieurs souhaits. Certains se sont réalisés, d’autres moins.


#### ~~Ĝis~~ cocarto

L’ensemble de l’enveloppe de 50 000 € votée lors de l’assemblée générale a été épuisée et les développements se font désormais « à crédit ».

Nous pensons que le produit a un réel potentiel. Nous échangeons avec plusieurs clients potentiels, mais pour l’instant un seul a validé le devis.

#### Le bateau

Il continue à remplir son rôle de se voir plus souvent, tisser des liens avec des personnes extérieures, accueillir les non-parisiens et a été particulièrement utile pour accompagner le stagiaire ayant travaillé sur cocarto.

Cependant, il y a un risque d’entre-soi : les personnes venant sur le bateau sont également celles qui participent le plus souvent au tour de table. Cependant le bateau sert pour les non-parisiens de point d’ancrage pour retrouver leur collègues.

#### Impression générale

En dehors de cocarto, il y a une impression de flottement général qui se caractérise notamment par l’absence d’organisation d’un séminaire pour l’AG, et l’absence de projet commun en dehors de cocarto.

Il n’y a pas eu de participation de notre part à des événements extérieur pour revendiquer le fait d’être une coopérative et d’aller contre les formes communes d’entreprise.

Ce flottement vient peut être de l’éclatement géographique de plus en plus important des membres.

#### Codecode.coop

Nous avions évoqué lors de la dernière AG une nouvelle manière de recueillir des témoignages que nous n’avons pas mis en place.

## Perspectives pour 2023

### Envie de se re-impliquer dans les mouvements

Par exemple dans « coop en luttes », dans des conférences, dans codecode.coop, ou bien publier des articles comme celui pour présenter notre charte d’éthique. Afin de re-créer quelque chose de vivant au sein de Codeurs en Liberté. 

## Autres mentions obligatoires

### Évènements importants survenus depuis la clôture
Néant

### Activité de recherche et développement
Néant

### Prise de participation
Néant

### Filiales et participations
Néant

### Dépenses non déductibles fiscalement
Néant

Conventions réglementées visées à l’article L. 223-19 du Code de commerce
Aucune convention relevant de l’article L. 223-19 du Code de commerce n’a été portée à la connaissance du gérant.