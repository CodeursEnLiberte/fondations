# Rapport de gestion 2020

L’année 2020 s’est terminée et, sans se presser, l’assemblée générale annuelle est arrivée. C’est l’occasion de faire un point sur ce qui s’est passé, ce dont on est content·e·s et ce sur quoi on sait qu’on est à la ramasse.

## Bilan financier

La référence est la liasse fiscale en annexe ; c’est elle qui fait foi et qui est soumise à l’approbation des sociétaires.

Nous présentons ici des chiffres simplifiés pour avoir une idée de l’activité.
* Chiffre d’affaires : 2 057 199
* Sous-traitance : 1 243 059
* Salaires : 700 486
* Dépenses de fonctionnement : 37 939
* Résultat bruts : 94 086
* Impôts sur les bénéfices : 22 563
* Résultat net : 71 523

Nous avons une marge brute de 4,5% ce qui correspond aux ordres de grandeur attendus.

Nos clients on été (par ordre décroissant de chiffre d’affaires) :
* DINUM
* Octo
* Fretlink
* Scopyleft
* Ada School
* Deepki
* Olvo
* Sons of Georges
* Brut
* 15marches
* Association for Progressive Communications

## Bilan moral

### La zone

En 2018, nous avions répondu (en groupement nommé [la zone](https://la.zone) avec [/ut7](https://ut7.fr/), [dtc innovation](https://dtc-innovation.org/) et [Scopyleft](http://scopyleft.fr/)) à un marché public de la  [DINUM](https://www.numerique.gouv.fr/dinum/) pour [beta.gouv.fr](https://beta.gouv.fr).

Nous sommes content·e·s de l’avoir fait, notamment parce que nous avons démontré qu’il est possible d’appliquer une marge raisonable (5%) et, _in fine_, avons contribué à modérer la dépense d’argent public.

Au delà du cout, notre autre ambition était de diminuer les souffrances qu’engendrent l’administratif. Nous pensons avoir réussi à diminuer la pénibilité pour les prestataires portés ; en revanche nous ne sommes pas sûr·e·s d’avoir réussi à diminuer la souffrance pour les agent·e·s administratif·ve·s du côté beta.gouv.fr.

Si la charge administrative a été beaucoup plus faible que ce que nous craignions en nous lançant, nous ne pensons pas être capables d’apporter de nouveauté (baisser un peu plus les marges, réduire le travail admistratif de agent·e·s de beta.gouv.fr). La zone continue sans nous son chemin avec beta.gouv… Bonne route !

### Quelques exemples de clients

Le temps travaillé sur des produits beta.gouv par les salarié·e·s de Codeurs en Liberté a diminué sensiblement cette année et continuera modestement en 2021 sur Transport, API entreprise, écosanté, et Démarches simplifiées.

Par contre, un embryon de projet entre la DINUM et Framasoft se met en place en cette fin d’année et peut-être que nous contribuerons à une amélioration d’outils libres financée par l’argent public, affaire à suivre, mais nous sommes bien motivé·e·s !

Nous avons également accompagné [Olvo](https://www.olvo.fr/) (coopérative de cyclo-logistique) sur du développement web. Une activité un peu différente de nos habitudes : une plus petite structure, du mentorat d’une équipe existante (plutôt que du développement), et l’occasion de pratiquer des tarifs réduits pour les structures qu’on aime bien.

En dehors du travail purement technique, il y a eu pendant la première moitié de 2020 de l’encadrement à la nouvelle école d’informatique parisienne [Ada Tech School](https://adatechschool.fr/), qui revendique de donner davantage de place aux femmes dans le secteur. Il y a espoir d’y reprendre l’encadrement quand les restrictions liées au covid seront assouplies…

Symbolique niveau chiffre d’affaire mais une activité qui change du technique et qui a du sens : de la traduction de l’anglais au français de contenus web pour l’organisation internationale [Association for Progressive Communications](https://www.apc.org/), qui milite pour des technologies numériques émancipatrices et contre les discriminations structurelles partout dans le monde.

### Formation et accompagnement

Nous avons accompagné des stagiaires presque toute l’année !

Trois stagiaires nous ont rejoint au cours de l’année (entre 4 et 6 mois), sur [transport.data.gouv.fr](https://transport.data.gouv.fr),
[Démarches Simplifiées](https://www.demarches-simplifiees.fr/) et
[API entreprises](https://entreprise.api.gouv.fr/), pour faire du fullstack, de l’administration système ou du traitement de données.

Nous avons apprécié l’échange, merci à elleux et aux équipes d’accueil ! Outre le côté technique, nous sommes content·e·s que des personnes en formation puissent s’essayer au fonctionnement coopératif. En revanche notre accompagnement n’était pas entièrement satisfaisant : la crise sanitaire nous a plutôt isolé, et l’accompagnement d’un stage à distance était plus difficile.

Financièrement, nous avons joué la facilité : leur travail n’a pas été facturé. C’était donc facile de proposer qu’une personne vienne travailler gratuitement. Par contre, iels n’ont été payé quasiment qu’au minimum légal pour un stage : 700 € par mois. Cela nous gène et nous réfléchissons au meilleur moyen de proposer au moins un salaire équivalent au SMIC pour les futur·e·s stagiaires.

Dans une optique similaire, nous réfléchissons au recrutement pérenne de personnes avec un passage par une phase « junior ». Pas facile : nous voulons assurer une année de SMIC et cherchons donc à facturer le travail de la personne à un tarif correspondant. Il faut aussi du temps et les compétences pour que la personne soit bien accompagnée. Nous tâtonnons donc un peu sur tous les fronts et avons de petites discussions itératives au fur et à mesure qu’un début de plan semble se dégager. À suivre pour les mois à venir !

### Diversité

Nous n’avons fait aucun progrès : les salarié·e·s sont une écrasante majorité d’hommes blancs, nous reproduisons les inégalités du secteur.

Notre mode de recrutement fondé sur la cooptation et les affinités avec des gens avec qui nous travaillons souvent déjà contribue à renforcer cette tendance. Les réflexions pour changer à ce niveau-là restent embryonnaires.

### Fonctionnement courant et conditions de travail

La comptabilité était en partie gérée par une personne qui a fait une pause de six mois. Ce petit coup de pression a finalement été l’occasion de se ré-approprier collectivement les tâches, conformément à notre souhait d’éviter toute spécialisation.

Notre fonctionnement revendiqué « chaque personne trouve et facture ses missions individuellement » a causé une pression importante à certaines personnes. Cela devient très difficile à supporter si on traverse une période fragile, une chute de motivation, ou si on a une pression financière comme un emprunt immobilier. Si la mission ne convient plus, il est difficile de réussir de trouver l’énergie d’en trouver une nouvelle. De plus, nous avons rarement l’occasion de travailler ensemble.

Cela fait d’autant plus apparaitre l’envie de chercher des contrats plus collectifs et d’être plus solidaires dans la coopérative.

### Codecode.coop

Nous avons créé ce site dans l’espoir d’aider l’émergence de nouvelles coopératives en informatique. Il contient la traduction d’un guide général et des témoignages d’autres coopératives.

Nous recevons des témoignages à petite vitesse, on aimerait que ce site se lance un peu plus ! Nous avons toutefois déjà eu quelques retours positifs de personnes qui envisagent soit de postuler dans une coopérative, soit d’en créer une, et cela fait plaisir.

### Répercussions du covid

Le travail à distance étant déjà très répandu pour les travailleur·se·s de Codeurs en Liberté, les restrictions liées au covid n’ont pas eu de répercussion significative sur le chiffre d’affaire.

Pour l’encadrement à Ada Tech School : c’est difficile d’encadrer les apprenantes à distance et les voyages sont déconseillés et pénibles à envisager. Cela a fait perdre la motivation.

Humainement, c’est difficile, et plein d’initiatives motivantes n’ont pas pu être faites : pas de séminaire en présentiel, pas de [semaine de contribution au Libre](https://www.codeursenliberte.fr/entreprise/contribution_au_libre/), pas de présentation à des évènements, pas de projets collectifs démarrés ou continués.

C’est pesant pour tout le monde de ne pas se voir physiquement. Un stagiaire de septembre à début janvier, a même fait tout son stage sans physiquement croiser une seule personne de Codeurs en Liberté.

Nous avons quand même fait un séminaire à distance étalé sur plusieurs vendredis après-midi d’affilée. Ça nous a fait du bien de se retrouver pour parler de quelques sujets collectifs de fond et faire les idiots sur [gather.town](https://gather.town/) (vive l’outil build et coincer quelqu’un contre un mur)… Mais ça ne vaut pas un séminaire en présentiel !

### Perspectives pour 2021

Nous espérons pouvoir enfin mettre en œuvre notre souhait d’accompagner dans la durée des personnes pour qu’elles gagnent en expérience et autonomie. Cela passera peut-être par le cadre d’une embauche en apprentissage.

L’automne 2021 marquera la fin du contrat avec la DINUM. Le chiffres d’affaires devrait être comparable à 2020, mais baissera probablement au delà.

Nous espérons que la fin des limitations de contacts physiques prendra fin et que nous pourrons à nouveau nous voir physiquement et avoir des moments plus chaleureux.

Nous souhaitons devenir plus proactifs pour récolter plus de témoignages et partager les différentes manières de posséder et gérer une entreprise.

Enfin, nous avons envie de nous lancer sur un produit qui serait le nôtre, nous permettant de travailler plus souvent ensemble.

## Autres mentions obligatoires

### Évènements importants survenus depuis la clôture

Néant

### Activité de recherche et développement

Néant

### Prise de participation

Néant

### Filiales et participations

Néant

### Dépenses non déductibles fiscalement

Néant

### Conventions réglementées visées à l’article L. 223-19 du Code de commerce

Aucune convention relevant de l’article L. 223-19 du Code de commerce n’a été portée à la connaissance du gérant.
