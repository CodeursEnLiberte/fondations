# Assemblée générale ordinaire 2021

* **Lieu** : au domicile de la présidente, 17 rue Keller et en visioconférence
* **Présents physiquement** : Antoine, Pierre, Nicolas, Thimy, Tristram, Vincent
* **Présents en visio** : Francis, Kheops, Thibaut
* **Excusés** : Etienne
* **Absents** : personne
* **Pouvoirs** : aucun
* **Heure de début** de l’assemblée : 15:06

## Résolutions

### Validation des comptes
Selon les chiffres de la liasse fiscale en annexe.

#### Résultat du vote

* Pour : 9
* Contre : 0
* Abstention : 0


### Affectation du résultat

Le résultat est de 71 523 € nets, après impôts sur les sociétés. Voici l’affectation proposée (en accord avec l’accord de participation) :

* Participation : 35 761,50 €, répartis au _pro rata_ du nombre de mois salariés dans l’année
* Report à nouveau : 35 761,50 €
* Réserve : 0 €
* Dividendes : 0 €

#### Résultat du vote

* Pour : 9
* Contre : 0
* Abstention : 0


### Quitus de gestion à la présidente

Nous sommes entièrement satisfaits de sa gestion et lui donnons le quitus.

#### Résultat du vote

* Pour : 9
* Contre : 0
* Abstention : 0


### Élection à la présidence

Thimy ne souhaite pas se représenter. Kheops s’est déclaré candidat.

#### Résultat du vote

* Pour : 8
* Contre : 0
* Abstention : 1

Kheops est élu président pour l’exercice 2021.

Sa rémunération en tant que président est de 0 € (zéro euros).


### Rachat des parts sociales d’Etienne

Conformément à l’article 14 des statuts, le rachat des 100 parts à 1€ doit être décidé en assemblée générale.

#### Résultat du vote

* Pour : 9
* Contre : 0
* Abstention : 0


### Mettre le contenu du site en copyleft

On nous a demandé de pouvoir reprendre un bout de notre charte éthique. Cependant notre site n’ayant aucune mention explicite de licence, cela est impossible.

Passons l’ensemble du site sous licence en CC-BY-SA.
La charte d’éthique est en CC0.

#### Résultat du vote

* Pour : 9
* Contre : 0
* Abstention : 0

## Accord de principe de financer des locaux physiques pendant un an

Nous avons envie d’expérimenter un local commun à Paris.
Si cela se fait, il faudra décider du financement, en particulier vis-à-vis des non-parisien·ne·s.

#### Résultat du vote

* Pour : 7
* Contre : 0
* Abstention : 2

### Accord de principe de financer un produit à nous

Utilisons le report à nouveau pour investir dans un produit que nous aimerions commercialiser. Le produit et la manière de nous répartir les montants sont à rediscuter lorsque l’investigation aura été effectuée.
Cette décision ne bloque pas toutes les envies de contribution à d’autres projets libre. Comme le par le passé, tout le monde est très vivement encouragé à passer du temps financé par codeurs en liberté sur du logiciel libre.

#### Résultat du vote

* Pour : 9
* Contre : 0
* Abstention : 0

# Clôture

L’ordre du jour étant épuisé et aucune autre résolution n’ayant été proposée, l’assemblée générale ordinaire s’achève à 16:28.
