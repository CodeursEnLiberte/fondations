# Recrutement - diversité

On manque toujours de diversité à Codeurs en Liberté.

## Recrutement

Est-ce qu'on recrute ? Un consensus s'établit sur « On ne recrute pas de personnes déjà comme la majorité des membres ; mais on cherche à recruter des profils différents ».
Nous accueillons en septembre Enkhé pour un stage de 6 mois, avec possibilité qu’elle intègre la structure à la fin si ça se passe bien.

## Accompagnement des juniors

On a du mal à accompagner des devs juniors sur des missions : ça demande beaucoup de temps et d'énergie, qu'on peut mettre pour un stage, mais difficilement après. C'est également difficile d'accompagner en remote (et la moitié de l'équipe n'est pas à Paris). Notre mode de travail en général ne rend pas spontanément facile de l'accompagnement.

Difficile d'imposer une personne junior dans une équipe où on est (question de sous) ; et difficile aussi d'imposer des personnes junior à une équipe dans laquelle on n'est pas.

## Comment ?

Comment recruter des profils plus divers ?

- Recruter des personnes moins junior ? (par ex. 42 ? Tout le monde n'est pas convaincu)
- Recruter d'autres métiers que le dev ; côté design par exemple ? (Historiquement on craignait que des métiers plus en tension que dev n'exercent un trop gros poids sur la solidarité interne de CeL ; mais après discussion, même si le design est effectivement plus en tension que le dev, ça semble un risque gérable aujourd'hui pour CeL, vu notre composition et le fond de roulement)
- Aller à nouveau proposer à des personnes spécifiques ?

## Qui

Dans les personnes à qui on pourrait proposer :

- Rébecca D.
- Claire Z. 
- Camille R. ?
- Agathe B. ?
- Fanny C. ?
- Lisa D. ?
