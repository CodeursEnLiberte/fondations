# Compte-rendu mini séminaire à Tours

Quand: du 05/10/2023 au 07/10/2023
Où: à Saint-Avertin, près de Tours, comme lieu facile d’accès depuis Nantes, Paris et Bordeaux
Qui: Nicolas, Pierre, Enkhe, Vincent, Thimy, Francis, Tristram
Invité: Touladi

## Changement de nom

* Est-ce qu’on fait un changement de raison sociale ou juste de marque ?
    * On penche plutôt un changement de marque le temps de voir
* Comment est-ce qu’on décide ?
    * Déjà un consensus entre nous
    * Si à la fin du séminaire il y a toujours consensus, on demande l’avis des absents
    * S’il y a unanimité, on convoque une AG exceptionnelle pour marquer le coup
* Comment on utilise le nom
    * Chaque personne est « une codeuse en liberté » ou « un codeur en liberté ». Le regroupement s’appelle « **Codeureuses en Liberté** »
    * On prend les noms de domaine codeurs/codeuses/codereuses
    * On change le site web et les factures pour mettre que codereuse
    * On fait un nouveau tampon (nécessaire de toutes façons pour la nouvelle adresse et SIRET)

## Codir-copil-cocarto

### Réalisations techniques
* Grands chantiers
    * Import (avec mapping de colonnes, ré-import/jointure…)
    * Séparation données tabulaire des données carto au format mvt
* Autres sujets
    * Verrouillages des colonnes
    * Amélioration de la page de présentation
    * email
    * choisir un username

### Perspectives
* On est à environ -20000€.
* Le contrat avec le collectif vélo devrait bien se faire.
* Il y a un complément potentiel de la BPI, mais sous conditions (embauche)
* [discussions en vrac sur ce qu’il faut faire]
* Nous autorisons un découvert de 50k€ sur le projet. qui nous l espérons ont vocation a etre remboursés
    * Plus de sous dépensés au dela de cette somme sur le produit.
* Nous décidons de mettre le turbo sur la partie commerciale, cette fois pour de vrai.

## Transmission HTML & CSS moderne
Formation interne

## Comment gérer la distance qui s’installe
* Les séminaires deviennent de plus en plus court (avant on prenait 18h de voyage pour aller en corse, maintenant tout le séminaire dure 18h, on n’est jamais toustes présents).
* Prendre le temps est difficile (même si un tour de table c’est que 15min, ça coupe, on oublie).
* L’idée de prendre 2h/mois pour faire du jeu-papotage est pas évident : on a toujours une priorité (client, famille, rush…).
* Beaucoup de coopératives se gardent un temps pour garder du lien.
* On a évoqué que lorsqu’on estimait le rapport TJM/salaire brut dans le privé qu’un jour par mois devait être consacré à la coopérative.


### Prime partage de la valeur
Après discussions :
    * On propose de verser le maximum de la prime en octobre
    * Si les absents sont d’accord, Tristram fait la paye de tout le monde au max de la prime
    
Alternatives
    * Payer uniquement le montant max qui a déjà été payé
    * Faire la compta pour transformation des primes versées en salaire ordinaire

Problèmes potentiellement bloquants
    * Gestion individuelle de l’ardoise chamboulée
    * Ne pas contribuer aux caisses sociales
    * Faut-il vraiment être plus royaliste que le roi et se sacrifier personnellement alors qu’on y a droit. En ce sens faudrait-il pas aussi renoncer à la prime déplacement ?
