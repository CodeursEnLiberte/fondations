# Bilan de l’année 2016

Il ne s’agit pas d’un bilan au sens comptable, car exceptionnellement (lié à la création de l’entreprise) le premier bilan ne sera que fin 2017.

## Des chiffres !

En dehors d’une ridicule mission, le très gros a été généré par @kheops2713. Cependant, on peut se faire une première idée des couts récurrents et voir la marge de manœuvre qu’on a pour la suite.

Voici donc des captures d’écran de Zephyr

![](Screenshot_from_2017-01-03_21-46-07.png)

Cette courbe, représente le résultat, donc ce qu’on a gagné comme argent (en prenant en compte les dettes & co).

Depuis Novembre, l’entreprise est donc financièrement dans le positif (cela est du au fait que mon PC valant plus de 500 €, il doit comptablement être amorti. Il n’a donc perdu que 200€ de valeur (6 mois sur 3 ans)).

Yay !

Si on regarde la trésorie, voilà ce qu’on a : ![](Screenshot_from_2017-01-03_21-48-03.png)

Ce qu’il faut y voir :

- l’état nous doit de l’argent. Normal : on a fait beaucoup d’achats (dont un gros PC), et on a pratiquement encaissé aucune TVA (les facturations à l’étranger en étant dispensées)
- *Codeurs en Liberté* me doit de l’argent (j’ai payé le PC sur mes deniers perso, alors qu’il appartient à l’entreprise)
On doit encore beaucoup d’argent aux organismes sociaux (normal, on paye par trimestre, et ça sera mi-janvier)
Mon PC est la seule immobilisation de l’entreprise. Si les huissiers débarquaient, ils seraient déçus
Même après déduction du capital, il nous reste plus de 1000€. On peut donc dire qu’on est légèrement sur-capitalisés.

### Dépenses par compte

En commun, nous avons dépensé 853€ (création, serveurs, zephyr et banque).

Tristram a le plus mauvais bilan avec 2160€ de dépenses pour 500€ générés.

kheOps est l’employé de l’année, le seul à avoir rapporté plus d’argent que dépensé.

## Ce qui a été réalisé en 2016

- Création de l’entreprise 💓
- Choix d’un outil de compta 🔢
- 3 heures de tête-à-tête avec la banquière 🙀 (après le refus de 2 autres banques)
- Ouverture d’un compte en banque à l’étranger qui pour l’instant n’a pas servi 🗺
- Choix d’une médecine du travail (après le refus d’une). Le médecin est apparemment sympa et aime notre nom 😑
- Choix d’une mutuelle d’entreprise 🤒
- Début de réflexion sur la mise en place d’un PEE 💰 💰 💰

## Perspectives pour 2017

### Comptabilité

D’ici au vrai bilan, il nous faudra un vrai comptable. Ça peut probablement attendre la fin de ma mission au ministère de l’intérieur, laissant ainsi 3 mois pour réparer toutes mes bêtises. Je ne sens pas d’urgence à en prendre un avant, et on s’économise quelques milliers d’euros dans l’affaire.

Vu les dépenses, le cout supplémentaire de la mutuelle (55€ par employé et par mois) devrait assez bien se passer, tout en gardant inchangés les 5% pour le pot à 🍪 et 5% pour les frais de gestion.

### Embauches

Pierre sera formellement embauché d’ici peu.

On a aussi réfléchi comment faire pour intégrer de nouvelles personnes fondations#15 (closed)

###

Pierre:

> Ah, dans les choses qui ont été faites en 2016, derrière la ligne "Création de l’entreprise", ça inclut aussi la mise en place d'un serveur par @kheops2713, la réalisation d'un site web par @tristramg, des cartes de visite, enfin tout le début de la communication d'entreprise, quoi 📢
> 
> Au vu des discussions de fin 2016, un des bons enjeux de 2017 sera sans doute les embauches et le grossissement, oui. Faudra voir comment on intègre des nouvelles personnes, si on se trouve un lieu habituel de travail (ne serait-ce qu'un café), comment on discute ensemble, comment on gère la disparité de genre qui se profile à l'horizon. Ça va être bien :simple
