# [Séminaire 2024-1] Revue de www

- Nicolas, Thomas, Thimy, Tristram, 

codeursenliberté.fr : est-ce qu’on voudrait mettre des choses à jour?


## En vrac

- [ ] La charte d’éthique n’est pas facilement accessible
  - et il y a plusieurs posts sur le sujet qui n’en sont pas.
- [x] Il manque une parenthèse à la fin.
- [ ] Le logo prend trop de place dans l’entête.
- [x] retirer le “(avec ou sans accent)” dans les bios
- [ ] ajouter des pronoms
- [ ] mettre à jour les adresses en codeurs/codeuses/codeureuses
- [x] retirer le twitter de thimy
- [ ] demander à etienne s'il veut supprimer son lien twitter (ou le mettre à jour) (et des pronoms)
- [x] retirer les clés pgp ?
- [ ] mettre à jour les photos des gens

## texte de la page qui-sommes-nous
- “Chaque Codeur en Liberté attache une grande importance au travail fait avec amour.”
  - osef
  - d’ailleurs l’amour c'est pas du travail (enfin si)
- “nous exigeons que chaque membre s’approprie l’entreprise”
  - c’est discutable
- “Partager nos connaissances.”
  - la section est maladroite
  - le passage “Par exemple, les décisions de nos assemblées générales, ou encore les échanges sur divers points de fonctionnement (et pas que le résultat final)” est ce qui compte.
- “Nous essayons de faire un grand effort pour documenter ”
  - ehhhh
- “malgré des membres et des clients aux langues maternelles variées”
  - c'est un peu bullshit?
- “Tous élevé·es en plein air et nourri·es au grain.”
  - bon, nouvelle blague, s'il vous plait.

## Landing page

- on veut pas une liste de compétences
- Vous souhaitez devenir membre ?
  - eeeeeh
- Formes d’intervention
  - chaussures croco
- Mettre en avant les réalisations
  - cocarto
  - betabetabetabeta
  - zenly
  - sncf réseau
  - ratp🕴️
  - IGN
  - geovelo
  - deezer
    - ranlarjan
  - tous les clients
  - valoche
  - la région IDF
  - les services du premier ministre 
  - l’euro métropole de strasbourg
  - enercoop
  - cargonautes
  - fretlink
  - ADA
  - “petites structures et service public”
  - 
- contournement de censure ?
  - (moi chuicho pour expliciter)
- Vous voulez mettre en œuvre une collaboration informelle et ouverte ?
  - tristram va préciser ce paragraphe

- ajouter des catégories au blog
- rendre la page catégories visible
- marquer les cross-posts comme tels?
  - au sujet des articles “Cartographie” écrits par tristram sur son blog perso.
- page legal
  - mettre à jour l’hébergeur sur la page legal
  - et le responsable de publication
  - rendre le lien du mail cliquable
  - le passage sur les cookies est faux
    - on a accès à rien, mais gitlab-pages oui? 

## Next

- Thomas a fait une PR pour la parenthèse.
- > nouvel atelier de nettoyage en mob.
