# [Séminaire 2024-1] Journal

La Maison du Lac, Saint-Avertin, du lundi 25 mars (mi-journée) au vendredi 29 (mi-journée). 

# Lundi

1. Maintenance d’Ardoise
  - [bump dépendances](https://gitlab.com/CodeursEnLiberte/ardoise/-/merge_requests/135)
2. IA
  - Pourquoi est-ce que tout le monde s’en fout?
3. [Infomaniak](02-infomaniak.md)
4. On a mangé quoi
  - des burritos (soyez pas dég)
5. [WWW](03-www.md)

# Mardi

1. Maintenance d’ardoise
  - [bump dépendances](https://gitlab.com/CodeursEnLiberte/ardoise/-/merge_requests/135) mergé
  - Changement d’app gitlab vers une app de groupe.
2. [WWW](03-www.md)
  - mise à jour des mentions légales
  - changement des bios pour supprimer les gitlab
  - changement des bios pour supprimer quasiment toutes les clefs pgp
3. Discussion sur notre contrat de travail
  - A priori, on est pas dans les clous vis-à-vis de notre convention collective.
  - Changer le statut en cadre pour tout le monde
4. Financer l'open-source
  - La contribution au logiciel libre fait partie de notre travail (on contribue à un commun)
  - Copie Publique : 1% du ÇA 
    - C’est pas tant que ça (à 150 jours travaillés par an, ça fait l’équivalent d’une grosse journée)
    - Essayer pour un an ?
  - Ouvrir gtfs-structures au sponsorship ?
  - Financer les bibliothèques de notre stack
  - Prendre officiellement la maintenance de https://github.com/dtolnay/serde-yaml ?
5. “Ouverture” en plénière
6. Ré-organisation du nextcloud
  - Francis et Antoine
8. Brouillon de rerédaction de la page d’accueil de www
9. Envoi du devis à Enercoop pour Pierre et Enkhé
10. [Redevenir Militants](04-redevenir-militants.md)
11. On a mangé du DAL au choux-fleur
12. Discussions autour de france billet
    - On ne souhaite pas vraiment participer
    - On tente de contacter un gros qui veut candidater (OCTO ?) pour mettre en relation avec le dev OTP
    - Éventuellement s’il y a un truc bien scopé pour nous, on pourrait participer, mais à priori non

# Mercredi

10:00 Ouverture

1. Francis est (candidat) président
2. Rédaction du contrat de Enkhé et créations de profil
    - au passage mise à jour des templates des procédures 
4. Échange (en visio) Étienne sur le harcellement
5. Achat des noms de domaine sur infomaniak avec kSuite pro pour toustes
    - codeureusesenliberte.fr
    - codeureusesenliberté.fr
    - codeusesenliberte.fr
    - codeusesenliberté.fr
    (en plus des existants codeursenliberte.fr et codeursenliberté.fr actuellement chez gandi)
6. Tarte aux asperges et banana-bread
7. Automatisation des notes de frais
    * La partie la plus fastidieuse est l'entrée manuelles des notes de frais dans Zefyr, mais pas de possibilité d'import automatique, ni par API, ni par CSV (mail envoyé au support et réponse négative)
9. Début de configuration de kSuite/infomaniak
10. Déjeuner
11. Discussion: [Réduire nos TJM](05-réduire-nos-tjm.md)
12. Accès à la médecine du travail pour tous.
13. Discussion: Implication mensuelle à la cogestion
    * Constat: il n'y a que Tristram qui fait la paperrasse
    * Frustration de Tristram: il y a des trucs qui trainent que personne ne prend. Dans sa tête les gens devaient consacrer 1 jour / mois à la coopérative mais c'est pas le cas.

    Actions:
      - On rajoute dans les taches du mois:
          - vérifier les factures impayées
          - relever le courrier
      - On découpe les taches du mois en sous catégories (2) et chaque mois on désigne plusieurs personnes pour s'occuper de chaque catégorie.
      - chaque groupe **doit** faire une passe sur les tickets fondations ouverts
      - On ouvre une page sur le wiki et on note qui a fait les tâches en dernier pour qu'iels ne soient pas redésignés tout de suite.
13. 17:30 Retro en conseil
  - en local et remote avec Kheops 😀
14. Replay d’une vieille présentation de capitaine train
  - “Garder les forces productives au cœur”.
15. Le coaching, ça sert à quoi?

## Jeudi

10:00 Ouverture

1. RDV Compta (Tristram et Antoine / Endrix)
    - 2h de rdv avec Endrix pour faire la cloture des comptes 2023 et lever les ambiguités / erreurs de compta
3. Rédiger une [trame d’interview](https://gitlab.com/CodeursEnLiberte/codecode.coop/-/merge_requests/37) pour codecode.coop
  - auto-passée entre nous
  - ça serait bien de mettre à jour
4. On a mangé une poelée de patates & carottes, salade et tarte aux pommes
5. État des lieux cocarto
  - discussions sur comment ça s’est passé
      - le dérapage financier
      - comment s’est fait la prise de décision (technique, du projet en lui-même)
  - décisions
      - ~~On ne paye presque plus N.T. en fixe, mais 100% de variable~~
          - Ca a été évoqué, mais finalement on continue à le payer pendant 3 mois avec comité à la fin
      - On a déjà arrêté de se payer
      - Désormais si on se paye, dans ardoise l’argent est marqué comme du CA normal, pas du pot à cookies
      - Le paiement de N.T. creuse la dette de cocarto
      - 10% du CA généré par Cocarto sert à remplir le trou (on ne crédite dans le tableau que 90% de ce qu’on reçoit)
      - On paye N.T. encore 3 mois (ça représente 1/3 de ce qu’on met en commun)
      - En juin on veut avoir rencontré des gens en synchrone.
          - Si en juin on n'a rencontré personne on arrete là la prospection.
          - Si on a rencontré des gens, on fait un comité, pour décider de la suite (mais l'arret est possible aussi)
6. Journée de la coopérative, deuxième partie
    - Constat : les personnes n’arrivent pas trop à s’impliquer
    - Proposition : on dit que les 500€ versés par mois correspondent à une journée de travail en commun
        - Modèle de proposition:
            - 1 jour par mois 
            - jour toute.s ensemble (genre le 2eme vendredi du mois)
            - Cette journée est obligatoire, on ne peut pas y renoncer (sauf exception)
            - Ça permet de traiter les compta courante, les tickets exceptionnels, jouer, participer à la maintenance des outils communs
7. Francis veut bien être candidat pour être président
8. Travail sur des pmtiles pour le découpage administratif de la France
9. Rédaction de la page /ideas
10. Rédaction des résolution d’AG
