# Réduire nos TJM

(pré-discussion en cours sur les frais, la redistribution, l’épargne.)

Tristram: je trouve ça immoral d’être dans les 10% plus haut de FR;
proposition pour l’exemple: plafond de salaire net à 90% de des salaires FR.

Arguments: 
- Vincent: solidarité au sein de la boite;
- Francis: confort financier de la boite permet une plus grande liberté d’impact;
- Tristram: ne faisons pas de charité
- Vincent: pour rappel j'étais déjà pas d’accord avec le salaire de base de 500€ de CeL, ça change la proposition de base.
- Thomas: c'est plus facile d’avoir un impact si on fait partie des 10 % plus riches
  (rappel sur l’historique des modèle de répartition de CeL.)
- Nicolas: il y a deux sujets: baisser nos TJM (pour le marché) ou baisser nos salaires (par morale)
- Vincent: On a pas eu de débat réel sur le passage à un salaire de base
- Nicolas: Effectivement, c’est parce que ça a été fait pour corriger le modèle précédent (avec un effet de pic buggué)
- Tristram: On avait pas assez réfléchi à ça quand on l’a mis en place, sur le fait que c’était un salaire de base.
- Tristram: On a un modèle qui ne fonctionne que parce qu’on est riche: ça serait bien que, même symboliquement, le modèle de CeL soit accessible à des petits salaires.
- Dans le cas de Enkhé, qui débute avec un tjm à peu près moitié de la plupart des autres de CeL, ça fait un salaire annuel de ~60000 et une contribution de 4000 aux pots à cookies
- http://codeursenliberté.fr/blog/quand_travailler/

## conclusion

- on change rien?
-> il faudra rediscuter de _quelle idéologie_
