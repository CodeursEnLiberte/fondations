# [Séminaire 2024-1] Ateliers
# [AG 2024] Ateliers et sujets de discussion

Ce pad liste les possibilités d'ateliers de discussion pendant l’AG 2024 de Codeureuses en Liberté. 

### Mode d'emploi :

- Mettez les sujets de discussion que vous aimeriez bien aborder ;
- Rajoutez toutes les idées d'atelier que vous aimeriez bien organiser ;
- Contribuez aux discussions en amont de l'atelier.
 
:::info
:bulb: C'est un bon endroit pour préparer les discussions, en particulier pour les personnes **en remote**.
:::

<hr>

## 🧑‍💻 Comment bosser sur nos outils et l’infra?
*Initié par Nicolas (Tristram a fait plein de trucs)*

-> discussion sur infomaniak, 

*Nicolas*: Le nextcloud est un peu en jachère, on est qu’a moitié en confiance sur le mattermost, on paye un gros serveur pour pas grand chose, et on avait dit qu’on partait de gitlab. Est-ce qu’on passe sur infomaniak?
Propositions:
- Faire une journée infra pendant le séminaire?
- Se payer pour bosser sur l’infra?

## 🎨 Redesign du site codeursenliberté.fr
*Initié par Nicolas*
Il faudrait rafraichir un peu les pages de présentation, le texte sur la page d’accueil date de 2018.

## 📚 Mettre à jour de la documentation

Mettre à jour le wiki sur fondations.

- *Pierre* : quels sont les points où on n'est pas à jour ?
- *Nicolas*
     
## 🧑‍🎨 Faire un nouveau logo?

### Discussion
*Initié par Nicolas*
  - *Pierre* : J'aime bien le logo actuel, et je me dis qu'au moment de mettre à jour le nom c'est bien de garder un élément de continuité. @Tristram c'était quoi ton idée derrière un changement de logo ? 
  - *Tristram*: c’est nicolas
  - *Nicolas*: Je voudrais bien le rafraichir un peu, peut-être le rendre plus lisible. Et puisqu’on ajuste notre nom, c’est l’occasion.

## Faire des stickers cocarto et/ou CeL
*Initié par Nicolas*


## 💸 Recontacter les fournisseurs de 2022 à qui on doit des sous

[Issue Github #188](https://gitlab.com/CodeursEnLiberte/fondations/-/issues/188)

## Baisser nos TJM
*Initié par Tristram*

## Comment mieux nous impliquer collectivement sur les tâches qui trainent
*Initié par Tristram*

## Comment militer à nouveau
*Initié par Tristram*

## Financement de projet opensource que l’on utilise sur nos missions
*Initié par Vincent*

## Solidarité avec les juniors
*Initié par Vincent*

## Partage des taches de compta régulieres
*Initié par Antoine*

## Prendre un apéro avec Geovélo
*Initié par Nicolas*

J’ai envoyé un mail à Gaël S.

## Fatigue numérique

*Initié par Thibaut*

## Demi-journée fixe mensuelle collective et une demi-journée individuelle flottante obligatoires pour participer aux tâches de la coopérative

*Initié par Tristram*

Sur l’idée : 218 jours travaillés selon notre convention collective

- 200 jours facturés (qui incluent les séminaires qui sont rémunérés)
- 12 jours de participation à la gestion
- 6 jours de formation

Le but est d’éviter les échec collectifs tels que :
- au moment d’envoyer les convocation à l’AG les résolutions ne sont pas rédigées
- il n’y a pas de relance sur les factures impayées
- on a pas payé des fournisseurs plus d’un an après avoir identifié et tracé le problème

## Maintenance d’Ardoise

*Initié par Nicolas*

Ardoise fonctionne à peu près tout seul, mais on peut faire un minimum de maintenance
- mettre à jour ruby/rails/les gems
- basculer l’app gitlab sur le groupe, a priori c’est possible maintenant https://docs.gitlab.com/ee/integration/oauth_provider.html#create-a-group-owned-application
- faire un tour des issues ouvertes
