# [Séminaire 2024-1] Infomaniak

- Tristram, Nicolas, Thimy

### Migrer vers Infomaniak

Voilà ce que l’offre kSuite d’infomaniak propose, et ce à quoi elle correspond dans ce qu’on a déjà:

1. Mails (fastmail)
  - ràs avec fastmail, c’est juste pour simplifier
  - on a vérifié la gestion des alias: chez infomaniak, les adresses communes sont des vraies boites, qui peuvent donc aussi envoyer des emails
  - certains aujourd’hui n’ont qu’un alias, ils auront une vraie boite (mais avec possibilité de redirection systématique)
  - c’était aussi parce que chez fastmail, on paye par boite
2. Nom de domaine (gandi)
  - Il y a eu du drama gandi, dont on est un peu indifférents; ce n’est plus le cool kid que c’était avant.
  - ? est-ce qu’infomaniak gère directement ses domaines?
3. Cloud fichiers (nextcloud chez scaleway)
  - On a des problèmes de gestion et de disque saturé
  - a priori la migration est assez simple
  - ? est-ce qu’il y a un truc mieux que le dossier partagé de NC? Dans notre cas, c’est quasiment le seul usage, on utilise presque par le stockage par utilisateur
4. Chat (mattermost chez scaleway)
  - Pas de grosse souffrance à l’usage 
    - (quelques downtimes, et Pierre a fait pas mal de maintenance)
  - La machine est beaucoup trop grosse (et nous coûte cher.)
  - La migration avec l’historique est peut-être touchy
  - ? quid des comptes invités?
  - Est-ce qu’on a toujours des trucs de ADA tech school?
    - (a priori non)
  - Infomaniak a l’air chaud pour tester la migration, Pierre a proposé de leur fournir un dump
  - de mémoire, les PJ étaient un peu pénibles à réimporter proprement.
5. S3 
  - Actuellement chez scaleway, pour le backup du nextcloud et pour les pièces jointes cocarto.
  - pas strictement partie de kSuite, mais infomaniak propose aussi ça
6. en bonus:
  - un jitsi
  - OnlyOffice

## Questions
- Est-ce que les apps mattermost/nextcloud habituelles sont compatibles?

## Pricing: 
- quel échelon chez infomaniak ?
  - ? peut-être qu’on doit choisir entreprise plutôt que pro pour les invités sur chat
- par mois: 3,29*11 = 36,19 (pro) ou 6,21*11 = 68,31 (entreprise)

- Actuellement, on paye
  - scaleway: on paye ~210 € par mois.
  - gandi: on a payé 343€ en un an (dons 100€ pour codecode.coop et 50€ pour cocarto.live)
  - fastmail: 7 users, $3.96/month per use

## Décisions

- Profiter du rebranding vers codeureuses pour faire une bascule.
- On a pas besoin d’attendre le retour d’Infomaniak sur les imports
  - On accepte un réimport imparfait (sans correspondance des utilisateurs)

## Actions

- > Acheter les noms de domaine codeureuses
  - CETTE SEMAINE (il y a un atelier pour décider ça)
- > Créer les comptes infomaniak
- > Transférer les données NC
- > (Quelques questions sur les migrations de boites IMAP)
- > Faire un blogpost “on change de nom mais on reste les mêmes”.
