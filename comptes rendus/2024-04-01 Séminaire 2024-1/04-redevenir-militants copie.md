## Redevenir Militants

- à titre personnel, on l’est tous un peu, mais à titre d’entreprise, moins.
- participer à de l’opensource
  - le militantisme, c’est de dire qu’on le fait
- dons à streetpress (et dons en général)
  - moins fait en 2024
- soyez adultes
  - (l'entreprise doit payer son utilisation de LL)

### Qu’est-ce qu’on a fait? (et qu’on ne fait plus?)

- présentation à pas sage en seine
- ainsi qu’à 42
  - on a probablement eu un peu d’impact chouette
  - -> on a contacté les descodeuses pour venir faire une présentation sur les coopératives
  - -> pierre “chez mon ecole de bourges” (l’ISEP) 
  - -> tristram ma prépa au lycée charlemagne
- semaine de contribution au libre
  - ça n'a pas eu tant d’impact
- codecode.coop
  - -> atelier à faire pour rédiger une grille d’interview
- les slides de la présentation chez 42 sont probablement réutilisables
- l’éthique en informatique
  - la charte
- militer hors de CeL
  - faire de l’informatique en bénévole dans des assos?
- mettre un bandeau “en grève” sur le site
- coopenluttes
- onestla.tech
- on a parlé de mouvement de grèves dans la newsletter cocarto.

Est-ce que choisir nos clients (PeS, le collectif VIF, cargonautes) est du militantisme?
  - faire du probono pour une asso, c’est du militantisme?
  - mettre à dispo nos moyens (par exemple, du stockage)?
  - accueillir une réunion de la CNT au bateau

- Publier nos action concrètes. 
- Tenir un journal de mini-actions: ça nous aiderait à nous rendre compte qu’_on fait des choses_.

- Est-ce que le militantisme fait chacun de son côté engage l’entreprise?
- Une entreprise peut servir à militer mais dans son périmètre, pour nous l’informatique, les coopératives. 
  - est-ce que par exemple, CeL pourrait investir dans du logement solidaire?
- Thomas: outiller par le numérique des assos militantes, c’est du militantisme
- Thimy: quand j’ai bloqué une déchetterie, j’ai le soutien de CeL
- Tristram: aucune de ces actions n’est spectaculaire, mais ça fait plein de petits points
- Pierre: on peut faire la publicité de ce qu’on fait “dans le domaine du travail dans l’informatique” mais le reste, on est pas forcément légitime
- Tristram: quand on fait des dons au logiciel libre, on a particulièrement envie d’en faire de la publicité.
- Parler des retraites, ça fait partie du _travail_.
- Accueillir des syndicats, ça fait partie du _travail_.
- Donner des coups de mains numériques à l’extérieur.
- Codeurs en Liberté n’a pas la possibilité d’aider sur des problématiques d’aide générale.
- Avis (indécis): donner à Exodus Privacy c’est le rôle de CeL. Donner pour l’Ukraine, moins.
- Jouer dans une fanfare pendant une manif contre la réforme des retraites, est-ce que c’est encore du _travail_ ?
- être derrière la bannière onestla.tech, oui, fanfare, non ?

Certaines structures séparent l’activité centrale et “l’association des amis”.
Est-ce qu’on ferait un deuxième cercle des copains autour de CeL?
(Est-ce qu’on ferait une asso pour défiscaliser des dons héhéhéhé.)
Tristram: Est-ce qu’on ferait quelque chose du nom codecode.coop ?
Thomas: J’ai l’impression d’avoir plus de contexte pas au contact de gens qui font de l’informatique.
Thimy: Chez bump par exemple, on fait un produit _pour les gens déjà dans l’informatique_, mais qui donc n’ont pas vraiment de problème.

Deux aspects:
1. aider (réduire la souffrance du numérique) des gens qui militent dans d’autres domaines
2. inciter le monde de la tech à être moins fucké

--
Entretien avec Etienne demain sur le harcèlement en ligne.

Comment outiller ? (Etienne a peut-être des billes ?)

--
Est-ce qu’on encourage tout le monde à _être syndiqué_?

--

## Actions

- tenir un journal des actions “militantes” passées 
  - privé
  - avec le passé
  - > Tristram
- parler d’adhésion aux syndicats
  - pour aller faire de l’entrisme à la CNT
  - > les syndiqués actuels
- présentation sur les coopératives
  - retourner faire des présentations
  - > Pierre (objectif: deux-trois écoles?)
- grille d’interview codecodecoop
  - > atelier ici-même
- entretien sur le harcèlement en ligne
  - > avec etienne demain
- soutien au logiciel libre (1% copie-publique) 
  - > Ajouter une résolution à l’AG
