# Assemblée générale extra-ordinaire

**Date** : 2022-02-01
**Lieu** : en visio-conférence

Présents :
    * Tristram Gräbener
    * Nicolas Bouilleaud
    * Francis Chabouis
    * Thimy Kieu
    * Pierre de La Morinerie
    * Vincent Lara
    * Thibaut Sailly

Procurations :
    * Aurélien Mérel à Tristram Gräbener
    * Antoine Desbordes à Nicolas Bouilleaud

Total des voix exprimées : 9

Début de séance : 16:04
    
## 1. Changement d’adresse

Le siège sociale de Codeurs en Liberté est déplacé au :
    57 quai de la Seine
    Bâtiment B2, boite aux lettres 32
    75019 Paris
    
Pour : 9
Contre : 0
Abstentions : 0

## 2. Mandat à Tristram pour les démarches

Tristram est mandaté pour effectuer toutes les démarches nécessaires pour effectuer le changement d’adresse.
Dans ce but, le Président donnera une procuration auprès de la poste.

Pour : 9
Contre : 0
Abstentions : 0

L’ordre du jour étant épuisé et qu’il n’y pas de point supplémentaire, la scéance est levée.
    
Fin de séance : 16:08


