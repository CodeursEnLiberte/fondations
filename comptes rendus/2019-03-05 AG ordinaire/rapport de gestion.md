# Rapport de gestion concernant les opérations de l'exercice 2018

Chers et chères sociétaires, chers curieuses et curieux,

Conformément à la législation, je me plie à l’exercice du rapport de gestion pour l’exercice 2018 clos le 31 décembre dernier.

Étant donné notre fonctionnement, vous avez accès en tant que sociétaire à l’ensemble des données comptables en toute transparence toute l’année.

La liasse fiscale est [jointe](liasse_fiscale_2018.pdf) à ce document.

## 1. Situation et activité de la société durant l’exercice écoulé

### Bilan financier

La liasse fiscale fait foi et c’est elle qui sera soumise à votre approbation.

Cependant, voici un résumé de l’exercice passé (en euros) :

* **Chiffre d’affaires** : 564 369
* **Salaire immédiat** (net et abondement pour l’épargne salariale) : 254 720
* **Salaire sociabilisé** : 188 056
* **Personnes portées** : 63 220
* **Dépenses communes** 
    * Taxes : 3 796
    * Prévoyance : 2 890
    * Complémentaire santé : 6 037
    * Banque : 1 633
    * Médecine du travail : 792
    * Frais de séminaire, invitations… : 6 000
    * Logiciels (zefyr, gandi, fastmail) : 1 483
* **Frais individuels** : 8 232
* **Dons** : 5000
* **Résultat brut** : 712
* **Impôts** : 0 (grace au don déductible)
* **Résultat net**: 712

### Membres de codeurs en liberté

Il y avait trois membres à la création de codeurs en liberté ; nous étions six il y a un an, et 10 aujourd’hui.
Francis, Étienne et Antoine nous ont rejoint courant 2018 et Thibaut le mois dernier ; d’autres personnes pourraient nous rejoindre dans les prochains mois.
Nous avons aussi accueilli un stagiaire (Baptiste) pour la première fois. C’était une opportunité, et c’était un succès sur tous les plans.
Par ailleurs, via le contrat avec la DINSIC, nous portons en ce moment 8 personnes, avec qui nous avons des liens plus ou moins forts.

### Clients

Nos clients ont été les suivants (1), par ordre de chiffre d’affaires décroissant : 

* DINSIC (beta.gouv et Etalab) :
  * par le marché public : 128 460 (2)(3)
  * Octo : 294 680 (4)
* Equalit.ie : 41 273
* Deepki (par talent.io): 23 692
* La Compagnie des Mobilités (Géovélo) : 19 400
* Société d'exploitation de l'Hebdomadaire le Point : 17 100
* Zen.ly : 16 000
* Munk School 13 163
* HBS RESEARCH: 6 565
* Solar Century : 1 400 (5)
* Centre de Recherches Historiques de l’EHESS : 833

L’écart entre le facturé et le chiffre d’affaire s’explique par des gains sur les taux de change

(1) D’après Zefyr>Facturation>Statistiques/Ventes/Clients.
(2) Les bons de commandes de la DINSIC n’apparaissent pas dans Zefyr, uniquement les factures, qui ne sont faites qu’au moment du « service fait ». Autrement dit, il y a un manque de quelques mois.
(3) Dont 4800 via /ut7 par erreur de la DINSIC
(4) Dont -10820 (en avoir) suite à une facture en double
(5) Jamais encaissé suite à un manque de motivation de la banque à essayer de comprendre où est passé le virement

### Perspectives et évolutions

En 2018, nous sommes passés d’un petit groupe d’indépendants à une vraie coopérative, avec un projet plus affiné, et nous pouvons affirmer que notre « modèle » d’entreprise fonctionne. Nous commençons, tout doucement, à avoir une petite notoriété.

Nous avons concrétisé quelques unes de nos envies.

Nous avons pris position, publiquement, en tant que Codeurs en Liberté, en soutenant des mouvements de protection des libertés numériques, et en adoptant une charte éthique.

L’année 2018 a été marquée par un changement dans notre relation avec la DINSIC, que nous n’avions pas vraiment prévu, même si nous l’avions imaginé. Le renouvellement du marché public « d’accompagnement agile » nous a permis de facturer l’État directement, sans être sous-traitant d’Octo Technology.

Ce marché public a aussi été l’occasion de constituer un groupement informel, « La Zone » (Codeurs en Liberté, /ut7, scopyleft et DTC innovation) pour faire une réponse solidaire, et construire une communauté. Le nom « La Zone » était un code l’an dernier pour une communauté des collectifs d’indépendants en informatique.

Notre relation avec la DINSIC, telle qu’elle existe aujourd’hui, est valable sur le papier jusqu’au renouvellement du marché dans 4 ans. Cela dit, c’est une administration en réorganisation permanente ; notre position au sein de beta.gouv.fr, et l’autonomie dont l’incubateur dispose nous permettront peut-être de contribuer à l’améliorer.

« La Zone » existe désormais en dehors de Codeurs en Liberté : ce n’est pas exactement ce qu’on imaginait, mais c’est un  collectif qui va avancer de lui-même.

Notre « Semaine de contribution au libre » mi-avril sera l’occasion de réellement travailler ensemble une semaine sur un projet commun, mais aussi de nous rapprocher de la Paillasse / MLC et de Framasoft.

Le projet de conférence « codecode.coop », est encore à préciser, mais est au programme de 2019.

Enfin, nous avons finalement résolu de passer la barre des 10 membres en gardant notre fonctionnement. D’autres libre-codeurs·euses vont nous rejoindre cette année ; il sera probablement temps de reparler d’organisation à un moment.

### Évènements importants survenus depuis la clôture

Néant

### Activité de recherche et développement

Néant

### Prise de participation

Néant

### Filiales et participations

Néant

### Dépenses non déductibles fiscalement

Néant

## 3. Conventions réglementées visées à l'article L. 223-19 du Code de commerce

Aucune convention relevant de l'article L. 223-19 du Code de commerce n'a été portée à la connaissance du gérant.

## 4. Affectation du résultat

Le résultat est de 712 €.

Conformément à l’accord de participation, 50% du résultat est affecté à la prime de participation.

Le reste est reporté à nouveau.

## 5. Résolutions particulières

### Choix d’un nouveau président

Mon mandat de président finit avec cet exercice. Conformément au mécanisme de présidence tournante de Codeurs en Liberté, je souhaite qu’un autre membre me succède à ce poste.

### Accord de participation

Conformément aux recommandations de la DIRECCTE, l’accord de participation existant fait foi pour cet exercice. Les ajustements demandés devront être entérinés (aujourd’hui même), mais ne changent pas les montants.

## Conclusion

J’espère que cette présentation de l’exercice passé ainsi que les résolutions vous conviennent et que vous me donnerez le _quitus_ pour la gestion de cet exercice.

Nicolas Bouilleaud
