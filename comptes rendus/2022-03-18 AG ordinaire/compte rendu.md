# Assemblée générale ordinaire 2022

Heure de début : vendredi 18 mars à 12:05

**Présents** :

* Antoine
* Francis
* Khéops
* Nicolas
* Pierre
* Thibaut
* Thimy
* Tristram
* Vincent

**Observateurs** :

 * Lydia
 * Thomas
 

## Résolutions

### Validations des comptes
Selon les chiffres de la liasse fiscale en annexe

#### Résultats du vote
 * Pour : 9
 * Contre : 0
 * Abstention : 0

### Affectation du résultat
Le résultat net après impôts est de 82 397 €. Voici l’affectation proposée (en accord avec les obligations de réserve légale coopérative et l’accord de participation) :

 * Dividendes : 0 €
 * Réserve coopérative : 3 363,10 € ce qui la porte à 10 000 €
 * Participation : 39 516,95 € (50% restant)
 * Report à nouveau : 39 516,95 €

#### Résultats du vote
 * Pour : 9
 * Contre : 0
 * Abstention : 0

### Quitus de gestion du président
Nous sommes entièrement satisfait·es de sa gestion et lui donnons le quitus.

#### Résultat du vote
 * Pour : 8
 * Contre : 0
 * Abstention : 1

### Élection à la présidence

Aurélien Mérel, président sortant, ne souhaite pas prendre un nouveau mandat.

Vincent Lara se présente comme unique candidat. Nous l’élisons président.

#### Résultats du vote
 * Pour : 9
 * Contre : 0
 * Abstention : 0

### Variations de capital
La durée de conservation de 5 ans pour la réduction fiscale étant passée, Tristram souhaite que Codeurs en Liberté rachète 1000 € d’action au prix nominal l’amenant à 1000 €.

Francis souhaite acheter 900 € de parts pour arriver à 1000 €.

#### Résultats du vote
 * Pour : 9
 * Contre : 0
 * Abstention : 0

### Accepter Thomas, Lydia en tant que sociétaires
S’iels souhaitent nous rejoindre, iels peuvent entrer au capital au cours de l’année 2022 à hauteur de 100 à 1000 €.

#### Résultats du vote
 * Pour : 9
 * Contre : 0
 * Abstention : 0

### Prendre un bail 3/6/9 à Nantes
Vincent souhaite qu’il y ait un local à Nantes, qui serait un bail commercial 3/6/9 au nom de Codeurs en Liberté et partagé avec d’autres personnes.

Financièrement, le but est que ce soit autosuffisant, mais le collectif est solidaire.

Nous soutenons cette initiative et engageons la coopérative pour les besoins administratifs.

#### Résultats du vote
 * Pour : 9
 * Contre : 0
 * Abstention : 0

### Investir 50 000€ dans ĝis
Ĝis est une idée de produit permettant la saisie collaborative de données tabulaires avec une sensibilité géographique et territoriale.

Les bénéfices accumulés dépassent les 150 000 €. On propose d’investir 50 000 € pour le développement de ĝis.

Une comptabilité spécifique sera mise en place pour pouvoir rendre des comptes sur les dépenses.

#### Résultats du vote
 * Pour : 9
 * Contre : 0
 * Abstention : 0

# Clôture
L’ordre du jour étant épuisé, la scéance est close à 13:01.
