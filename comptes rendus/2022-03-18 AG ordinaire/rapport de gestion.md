# Rapport de gestion 2021

## Bilan financier
La référence est la liasse fiscale en annexe ; c’est elle qui fait foi et qui est soumise à l’approbation des sociétaires.
Nous présentons ici des chiffres simplifiés pour avoir une idée de l’activité.

 * Chiffre d’affaires : 2 477 753 €
 * Sous-traitance : 1 728 534 €
 * Salaires : 401 159 €
 * Salaires chargés : 602 006 €
 * Dépenses de fonctionnement : 36 956 €
 * Résultat brut : 99 839 €
 * Impôts sur les bénéfices : 17 443 €
 * Résultat net : 82 397 €

Le bilan est comparable à celui de l’année dernière.
Nos clients ont été (par ordre décroissant de chiffre d’affaires) :

 * DINUM (y compris au travers de Scopyleft, Octo et Malt) 
 * Kisio Digital
 * Fretlink
 * RATP
 * leboncoin
 * 00h00
 * BZIP diffusion


## Bilan moral

### La zone
En 2018, nous avions répondu (en groupement nommé la zone avec /ut7, dtc innovation et Scopyleft) à un marché public de la  DINUM pour beta.gouv.fr.
L’assemblée générale ordinaire de 2021 avait acté que nous ne candidaterions pas à nouveau.
Le résultat est une marge très significative, dont l’utilisation doit être décidée au cours de l’assemblée générale.
Bilan financier de La Zone
Nous avons facturé au total 4 870 350 € à la DINUM.

Les marge de 5% (243 517,50 €) a été utilisée de la manière suivante :
 * Frais de mouvements bancaire : 15 000 €
 * Clause sociale : 48 469 €
 * Impôts sur les sociétés environ 65 000 €

La Zone nous a donc rapporté environ 115 000 €

### Formation et accompagnement
Nous n'avons eu qu’une seule stagiaire, cependant nous avons enfin décidé que l’indemnité serait au niveau du SMIC et non pas du minimum légal.
Nous avons également accueilli une apprentie pendant un an.

### Diversité
Comme à chaque assemblée générale, nous constatons que nous restons significativement plus mauvais que les autres SSII. Nous sommes 2 femmes sur 11 salarié·es. Deux personnes se considèrent racisées.

### Fonctionnement courant et conditions de travail
Le bateau a été un chouette évènement permettant de se voir plus souvent, aussi bien entre parisien·nes que pour accueillir les non-parisien·nes ou encore croiser des codeur·euses ami·es.

### Retour sur les perspectives annoncées prévues pour 2021
Lors de l’assemblée générale ordinaire de 2021, nous avions émis plusieurs souhaits. Certains se sont réalisés, d’autres moins.

#### Embauche d’une personne en apprentissage
Nous l’avons fait. Un bilan doit être fait pour savoir si notre accompagnement est utile et comment il pourrait être amélioré

#### Avoir un local afin de pouvoir se croiser physiquement plus souvent
Nous avons depuis août le bateau ! Il nous permet effectivement de nous voir un peu plus souvent, et également d’accueillir les non-parisien·nes de passage. Il permet aussi d’accueillir des amis proches et entretenir des dialogues sur nos relations au travail.

#### Lancer un produit à nous
Nous n’avons pas lancé de produit à nous, mais nous avons identifié un projet () et des tests techniques ont été initiés.

#### Codecode.coop
Nous n’avons pas réussi à récolter plus de témoignages pour codecode.coop mais il y a eu quelques échanges avec des coopératives. Par ailleurs le site a été mentionné dans des discussions à beta.gouv.fr et à Ada School.


## Perspectives pour 2022

Développer Ĝis et avoir un premier utilisateur ou une première utilisatrice

Changer la manière de récolter des témoignages pour codecode.coop : par exemple créer une trame que les autres coopératives peuvent remplir.


## Autres mentions obligatoires

### Évènements importants survenus depuis la clôture
Néant

### Activité de recherche et développement
Néant

### Prise de participation
Néant

### Filiales et participations
Néant

### Dépenses non déductibles fiscalement
Néant

Conventions réglementées visées à l’article L. 223-19 du Code de commerce
Aucune convention relevant de l’article L. 223-19 du Code de commerce n’a été portée à la connaissance du gérant.
