# Assemblée Générale 2024: Rapport moral sur l’année 2023

## Le bateau

Le bateau continue d’accueillir des non-parisien·nes pour la nuit et des stagiaires pour un accompagnement plus suivi.

Il permet aux parisien·nes de se voir régulièrement dans un lieu fixe, et de faire des réunions pour échanger sur les coopératives et autres sujets.

Au cours de l'année, la bateau a couté 10 283,95 € à Codeureuses en Liberté dont 6 600 € ont été financés par les parisiens (Tristram, Nico, Antoine, Pierre et Thomas).

## Problème d’implication

L’année a été marquée par des problèmes récurrents d’implication. Ainsi il est toujours très difficile de planifier un séminaire, même en anticipant de plusieurs mois ou encore de recueillir les ressentis afin de rédiger le rapport moral ou encore les résolutions à soumettre au vote.

Nous avons tenté de lancer régulièrement un jeu (en ligne, le vendredi soir afin de pouvoir concilier le distanciel sur tous les fuseaux horaires) afin relancer une dynamique commune.

Une résolution sera proposée à l’assemblée générale pour tenter de re-créer du lien et de l’implication.

## Formation et accompagnement

Nous n’avons accueillis cette année qu’une seule stagiaire de septembre à février, Enkhe Deny.

Cet accompagnement s’est fait totalement par Codeureuses en Liberté, contrairement aux années passées où les stagiaires étaient très souvent dans l’environnement beta.gouv.fr.

Ce stage sur cocarto s’est bien passé, nous avons proposé à Enkhe de nous rejoindre à la suite de ce stage, elle a accepté.

Nous sommes contents d’enfin accueillir une personne avec un profil plus junior, bienvenue Enkhe !

## Séminaires

Nous avons organisé deux séminaires cette année à St-Avertin, dans deux maisons différentes.

La meilleure anticipation du choix des date nous a permis une organisation plus fluide que les années passées.

Il semble plus difficile que les années précédentes de bloquer une semaine entière pour se voir, mais nous arrivons quand même à presque tous nous croiser.

## cocarto

Cocarto a représenté un investissement total de 158 000 €, un montant bien supérieur au montant initalement prévu de 50 000 €.

Le projet a été reconnu pertinent par la BPI, ce qui nous a permis de recevoir une subvention de 50 000 €.

Le premier contrat rémunéré a été signé cette année avec le collectif vélo Paris en Selle, pour un montant d'environ 10 000 €.

Nous n'avons pas réussi à signer d'autres contrats, probablement à cause de l'absence d'un commercial dédié.

Au cours de l'année, quelques dizaines d'utilisateurs ont créé un compte et ont au moins créé une carte avec un point dessus.

## Contrôle URSSAF

Nous avons fait l’objet d’un contrôle URSSAF au cours de l’année qui a mené un redressement d’environ 16 000 € principalement dû à un forfait mensuel repas trop généreux et à certaines personnes ayant refusé de toucher la prime de partage de la valeur.

Notre apprentissage principal est qu’il faut qu’on mette plus de choses par écrit (tout particulièrement si certaines actions ont des conséquences différentes d’un·e salarié·e à un·e autre).

## Galères de sous

Il s’agit à la fois d’un problème de trésories et de pertes comptables ce qui a été une source de stress en toute fin d’année 2023 et aussi au début de l’année 2024.

### Pertes comptables

- cocarto
- SNCF (sera bientôt résorbé)
- variation d’ardoise (50 000 €)
- fin de la vache à lait beta.gouv.fr avec les 5% que nous prenions sur chaque facture
- redressement URSSAF (16 000 €)

### Problèmes de trésorerie

Ce problème a été très aigü en février 2024, même si les causes datent de 2023 :

- pertes comptables
- factures impayées
- SNCF qui paye à 60 jours

2024 devrait voir remonter la caisse à son niveau nominal (autour de 300 000 €)

Pour information, voici les soldes bancaires en fin de mois :

- janvier 2024 : 84 390
- décembre 2023 : 101 475.73
- novembre 2023 : 222 134.62
- octobre 2023 : 222 355.84
- septembre 2023 : 263 053.19
- aout 2023 : 308 582.67
- juillet 2023 : 302 933.18
- juin 2023 : 306 276.94
- mai 2023 : 297 223.49
- avril 2023 : 289 678.00
- mars 2023 : 315 227.21
- février 2023 : 285 270.94
- janvier 2023 : 285 896.89
- décembre 2022 : 326 515.80
- novembre 2022 : 335 270.72
- octobre 2022 : 355 140.12
- septembre 2022 : 374 842.66
- aout 2022 : 384 498.34
- juillet 2022 : 322 227.35
- juin 2022 : 303 450.33
- mai 2022 : 320 894.81
- avril 2022 : 285 136.33
- mars 2022 : 258 934.57
- février 2022 : 277 725.49
- janvier 2022 : 246 477.65

## Encourager la création d’autres coopératives

Depuis la décision en Corse, peu d’efforts ont été fait en ce sens. Par exemple aucun témoignage n’a été recueilli pour codecode.coop et nous n’avons fait aucune présentation publique.

## Endrix notre comptable

Suite au contrôle URSSAF, nous avons décidé de faire appel à un cabinet comptable. Nous avons choisis Endrix.

À la fin de 2024 nous pourrons dresser un bilan de leur accompagnement.

## Perspectives pour 2024

### Fonctionnement commun

Nous espérons que les pistes proposées au séminaire début 2024 permettront une meilleur appropriation des sujets tranverses au delà de la paperasse quotidienne.

### cocarto

Nous allons y consacrer beaucoup moins de temps, mais le travail commercial continue afin de trouver des pistes pour re-financer le projet.

### Re-démarrage de codecode.coop

Nous avons écrit une trame d’entretien. Nous espérons qu’elle permettra d’obtenir plus facilement des témoignages d’autres coopératives.

Nous souhaitons également transmettre à nouveau dans des écoles, formations, conférences des formes alternatives de travailler en informatique.

### Nouveau client Enercoop

Après plusieurs rapporchements ratés (mauvais moment de part et d’autres), trois codeureuses travailleront desormais chez enercoop. Nous sommes très contents de pouvoir travailler avec une autre coopérative.

### Redressage des comptes

Nous arrêtons le financement sur fonds propres de cocarto. En parallèle, les nouvelles missions pour 3 personnes, la récupération de factures impayées et la normalisation du contrat avec SNCF devrait remettre nos finances à flot.
