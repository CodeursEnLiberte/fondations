# Déroulé et compte-rendu de l’Assemblée Générale 2024 (pour l’exercice 2023)

[Voir le compte rendu de l’AG 2023](https://gitlab.com/CodeursEnLiberte/fondations/-/blob/main/comptes%20rendus/2023-05-04%20AG%20ordinaire/compte%20rendu.md)

Date et heure de début : Vendredi 12 juillet 2024 à 16hxx

**Présent·e·s** :

* Nicolas Bouilleaud
* Enkhé Deny
* Antoine Desbordes
* Tristram Gräbener
* Thomas Guillet
* Thimy Kieu
* Vincent Lara
* Aurélien Mérel
* Thibaut Sailly

**Procurations** :

* Francis Chabouis a donné procuration à Tristram Gräbener

**Absent·e·s** :

* Pierre de la Morinerie



## Résolution 1 : Validations des comptes

Selon les chiffres de la liasse fiscale en annexe.

### Résultats du vote

| Pour | Contre | Abstention |
| ---: | -----: | ---------: |
|    9 |      0 |          0 |

## Résolution 2 : Affectation du résultat

Le résultat net après impôts est de 22 228 €. Voici l’affectation proposée (qui suit la recommandation du cabinet d'expertise comptable) :

 * Dividendes : 0 €
 * Réserve coopérative : 0 €
 * Report à nouveau : 22 228 €

### Résultats du vote

| Pour | Contre | Abstention |
| ---: | -----: | ---------: |
|    9 |      0 |          0 |

## Résolution 3 : Quitus de gestion du président

Nous sommes entièrement satisfait·es de sa gestion et lui donnons le quitus.

### Résultat du vote

| Pour | Contre | Abstention |
| ---: | -----: | ---------: |
|    9 |      0 |          0 |

## Résolution 4 : changement de dénomination sociale

Les statuts sont modifiés pour changer la dénomination de « Codeurs en Liberté » vers « Codeureuses en Liberté ». Les marques commerciales « Codeurs en Liberté » et « Codeuses en Liberté » pourront continuer à être utilisées.

*Motivation* : refléter la diversité de l’entreprise

*Modalités* : nous achetons en plus les noms de domaine `codeusesesenliberte.fr`, `codeusesesenliberté.fr`, `codeureusesenliberte.fr`, `codeureusesenliberté.fr`.

### Résultat du vote

| Pour | Contre | Abstention |
| ---: | -----: | ---------: |
|    9 |      0 |          0 |

### Résolution 5 : changement d’objet social

L’objet social est modifié de « La formation, étude, conseil, prototypage et développement en informatique. » vers « Développement en informatique et les activités annexes telles que formation, étude, conseil ou encore prototypage ».

*Motivation* : on souhaite faire changer le code APE afin de lever des ambigüités de classification de notre activité et lever des risques tels qu’être ré-attribué à une autre convention collective.

### Résultat du vote

| Pour | Contre | Abstention |
| ---: | -----: | ---------: |
|    9 |      0 |          0 |

### Résolution 6 : refaire les contrats de travail

Tous les salarié·es sont des cadres échelons 1.1 de la convention Syntec.

Le salaire de base (hors prime de performance) est de 25 000 € bruts annualisés.

Le temps de travail est de 35h par semaine.

Il est précisé dans les contrats que le chiffre d’affaires à générer est hors-taxes.

*Motivation* : Le changement d’échelon permet de respecter l’obligation de salaire minimal de la convention collective syntec.

### Résultat du vote

| Pour | Contre | Abstention |
| ---: | -----: | ---------: |
|    9 |      0 |          0 |

### Résolution 7 : Élection à la présidence

Thomas Guillet, président sortant, ne souhaite pas prendre un nouveau mandat. Francis Chabouis est l’unique candidat à la présidence pour une année.


Son activité en tant que président ne sera pas rémunérée. L’intégralité de ses revenus devra provenir de ses activités de développement informatique.

### Résultat du vote

| Pour | Contre | Abstention |
| ---: | -----: | ---------: |
|    9 |      0 |          0 |

### Résolution 8 : formalisation d’une journée de contribution aux outils internes

Chaque salarié·e consacre une journée par mois pour les activités en commun qui bénéficient à tout le monde (administration des services numériques, rédaction de textes, initiatives, suivi des tickets…). Cette journée n’est pas facultative.
Ce temps est reparti entre :
-  Une demi journée sera effectuée toustes ensemble, à distance, le premier vendredi ouvré de chaque mois, l'après midi,
-  Les tours de table,
-  Du temps personnel,
-  Des temps en petits groupes.

*Motivation* : de nombreux sujets transverses (tels que relancer les factures impayées, maintenir l’infrastructure…) ne sont pas pris par manque d’implication.

### Résultat du vote

| Pour | Contre | Abstention |
| ---: | -----: | ---------: |
|    9 |      0 |          0 |

### Résolution 9 : Attribution de 1% du CA 2023 au financement de l'open-source

Sur le modèle de [Copie Publique](https://copiepublique.fr/), 1% du chiffre d'affaires 2023 est attribué au financement de projets et logiciels open-source. L'attribution sera faite au cours de l'année 2024 par un processus collaboratif des sociétaires.

### Résultat du vote

| Pour | Contre | Abstention |
| ---: | -----: | ---------: |
|    9 |      0 |          0 |

## Clôture

L’ordre du jour étant épuisé, la séance est close à xxx.

## Rapport de gestion

### Bilan financier

### Autres mentions obligatoires

#### Évènements importants survenus depuis la clôture
Néant

#### Activité de recherche et développement
Néant

#### Prise de participation
Néant

#### Filiales et participations
Néant

#### Dépenses non déductibles fiscalement
Néant

#### Conventions réglementées visées à l’article L. 223-19 du Code de commerce
Aucune convention relevant de l’article L. 223-19 du Code de commerce n’a été portée à la connaissance du gérant.
