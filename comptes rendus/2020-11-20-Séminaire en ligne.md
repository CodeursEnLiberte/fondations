# Séminaire en ligne CeL 2020-11

## Organisation

On a fait une "Session 0" d'une heure sur gather.town le vendredi 13 novembre.

Pour les sessions suivantes, on propose **3 heures** le **vendredi après-midi** (prochaine session le 20 novembre, donc).

Pour se retrouver : https://gather.town/app/cPjCDvITkDzPwzd7/codeurs-en-liberte

## Sujets possibles

- Antoine : Comment mieux faire tourner les taches comptables ?
  VL, PLM
- Antoine : Jeu de société ?
  VL
- Nicolas : Coder sur Ardoise ou sur chuis.la
  TG (chuis.là), AD, VL (Embarquement ardoise), N, PLM
- Nicolas : Mario Kart
  AD, mais j'ai pas de switch (télétravail chez TG ?), N, H
- Nicolas : Participer à des mouvements: onestla.tech?
  N, H
- Vincent : Réponses aux marchés publics
  AD, N, PLM
- Vincent : Projet commun
  AD, N
- Vincent : Avoir un nom plus inclusif
- Francis : Ça devient quoi un vieux developpeur ?
  AD, VL, TG
- Antoine : Stage, est ce qu'on change quelque chose ? Rémunération ?
  PLM
- Antoine : Recrutement ?
  PLM    

## Programme

### Session 1

- 15 h : La suite de codecode.coop
- 16 h : Marchés publics
- 17 h : Apéro

### Session 2

- 15 h : coder sur Ardoise
- 15 h 30 : ça devient quoi un vieux développeur ?
- 16 h : maintient de salaire si on est malade/pas bien
- 17 h : Apéro

### Session 3


- 15 h : projet commun ? financé par le pot commun ?
- 16 h : comment utiliser notre argent en trop pour être solidaire financièrement dans CeL (poursuite discussion maintient de salaire si malade)
- 17 h : stages : on change quelque chose ? Rémunération ?

## Notes

### Marchés publics

On y est allé parce que :

- Lien avec d'autres coops
- Montrer qu'on peut répondre à un marché
- beta.gouv est cool
- Défoncer Octo, montrer qu'on peut prendre moins de marge

#### Certaines raisons de l'incompréhension

Certaines personnes de la Zone comprennent pas pourquoi on tire les prix vers le bas ; nous on comprend pas pourquoi ils facturent plus, en se disant qu'ils vont faire des trucs bien (mais sans rendre de comptes).

Les  gens de la zone qui n'avaient pas travaillé avec beta.gouv avant voulaient y apporter des méthodes agiles ; ceux qui avaient déjà travaillé voulaient juste encourager l'auto-gestion qui était déjà en place.

#### Est-ce qu'on veut continuer ?

Va y'avoir deux marchés :

- beta.gouv (interministérieur)
- Le Ministère de la Transition Écologique. _Pour le MTE, on ne sait pas encore à quoi ressemblera le marché (TJM, s'il y aura plusieurs attributaires et on peut se défausser, etc)_

T : Sur où est-ce qu'on décide de travailler individuellement, on se ferme sans doute aucune porte, même sans répondre au marché. Après, sur le marché, on a sans doute fait ce qu'on pouvait faire, mais c'est pas notre vocation.

P : ça serait intéressant de continuer sur le marché, mais on peut mettre notre puissance d'agir mieux ailleurs (semaine de contribution au libre, séminaires, etc)

A : ne se sent pas de prendre du temps là dessus sans être à beta.gouv.

#### Décision

- On ne participe pas formellement au renouvellement du marché beta.gouv.
- On n'a plus envie de faire du portage.
- On est chaud pour mettre le nom de CeL dans l'appel d'offre (en sous-traitant par exemple) pour garder une présence dans le groupement.

