Assemblée générale extraordinaire n°2

# 30 mai 2017

Présents : Pierre, Thimy, Nicolas, Vicent, Tristram, kheops (en visio).

L’ensemble des sociétaires étant présents, une assemblée générale
spontanée est organisée.

Début de l’assemblée générale : 19h10 heure de Paris.

## Résolution numéro 1

Le président (Tristram Gräbener) peut souscrire à la complémentaire santé.

Cette résolution s’applique même en cas de changement de président.

* Pour : 3
* Contre : 0
* Abstention : 0

Résolution adoptée à l’unanimité.

## Résolution numéro 2

Le montant minimum exigé pour qu’un employé devienne sociétaire est fixé
à 100 actions, sans aucune prime d’émission.

À titre d’information, cela revient à 100€ aujourd’hui.

* Pour : 3
* Contre : 0
* Absention : 0

Résolution adoptée à l’unanimité.

## Résolution numéro 3

Les employés Thimy, Nicolas et Vincent souhaitent devenir sociétaires,
à raison de 100 parts chacun.

L’assemblée se prononce sur leur acceptation :

* Pour : 3
* Contre : 0
* Abstention : 0

Résolution adoptée à l’unanimité.

Applaudissements et réjouissances.

## Fin de l’assemblée générale extraordinaire

L’ensemble des points étant épuisés l’assemblée générale est levée à 19h36.

## Réunion de gestion ordinaire

### Utilisation du pot à cookies

L’employé souhaitant participer à projet devra définir le projet sur
lequel il a envie de contribuer et la forme de la contribution, sans
pour autant donner de feuille de route précise.

Le projet doit être approuvé par consensus et donne droit à 1000€. 

### Financement de conférences

Chaque employé a droit au financement d’au moins d’une conférence par an.

Les modalités (déplacements, transport…) seront étudiées au cas par cas.

Codeurs en liberté financera le restant à charge pour la conférence
_Les rencontres régionales du logiciel libre_ à Nantes pour kheops.

### Journée coop

Discuté dans https://gitlab.com/CodeursEnLiberte/fondations/issues/20

### Discusions diverses


