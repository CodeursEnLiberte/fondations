# Assemblée Générale Extraordinaire du 10 mai 2022

## Présents

 * Thomas Guillet
 * Thibaut Sailly
 * Thimy Kieu
 * Tristram Gräbener
 * Aurélien Mérel
 * Nicolas Bouilleau
 * Vincent Lara
 * Pierre de la Morinerie


## Procuration
 * Antoine Desbordes à Nicolas Bouilleau

## Absent
 * Francis Chabouis
  
## Lieu

En visioconférence

## Début de l’assemblée générale

15:09
  
## Ordre du jour
###  Changement de domiciliation

Seul le domicile du/de la président·e peut accueillir le siège social de Codeurs en Liberté.

La nouvelle adresse chez Tristram Gräbener n’est donc pas possible.

Nous souhaitons domicilier l’entreprise dans une entreprise de domiciliation :

Sofradom
118-130, avenue Jean Jaurès
75019 Paris

Tous les pouvoirs sont donnés à Tristram Gräbener pour effectuer les démarches de domiciliation et pour toutes les procédures légales associées (greffe, etc.).

### Vote

* Pour : 9 voix
* Contre : 0
* Abstension : 0

## Clôture

L’ordre du jour étant épuisé, l’assemblée générale extra-ordinaire est levée à 15:10
