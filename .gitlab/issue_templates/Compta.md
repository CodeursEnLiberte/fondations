📚 [Voir le guide sur le wiki](https://gitlab.com/CodeursEnLiberte/fondations/-/wikis/Compta:-opérations-mensuelles)

## Au fil de l'eau

* [ ] Faire les virements de salaires
* [ ] Enregistrer la DSN [dans le drive](https://kdrive.codeureusesenliberte.fr/app/drive/1055170/files/1241)
* [ ] Associer les frais / virements / factures dans [Tiime](https://apps.tiime.fr)
* [ ] Téléchargement et mettre de le [drive](https://kdrive.codeureusesenliberte.fr/app/drive/1055170/files/656) l’extrait des comptes du mois

## Comptabilité générale

### Relancer les factures impayées

* [ ] Vérifier s’il y a de nouvelles factures impayées

### Traiter les sujets courants

* [ ] Faire une passe sur tous les [ticket ouverts](https://gitlab.com/CodeursEnLiberte/fondations/-/issues)
* [ ] Faire une passe sur toutes les [MR ouvertes](https://gitlab.com/CodeursEnLiberte/fondations/-/merge_requests)

## Meta

* [ ] Désigner les 2 binômes pour le mois suivant (les premiers de [cette liste](https://gitlab.com/CodeursEnLiberte/fondations/-/wikis/Compta:-op%C3%A9rations-mensuelles#32-d%C3%A9signer-le-bin%C3%B4me-pour-le-mois-suivant))
* [ ] [Créer l’issue pour le mois suivant](https://gitlab.com/CodeursEnLiberte/fondations/-/issues/new?issuable_template=Compta) :heart:
* [ ] Relever le courrier
