# Convert the specified markdown files to good-looking pdfs

DECISIONS= $(wildcard décisions-d’entreprise/*.md)
pdf: statuts.pdf $(DECISIONS:.md=.pdf)

%.pdf: %.md
	pandoc -V geometry:margin=3cm --output="$@" "$^"
