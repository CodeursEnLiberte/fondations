:wave: Bienvenue !

Le projet CodeursEnLiberte/fondations> regroupe nos discussions et documente notre fonctionnement. Nous sommes *ouverts par défaut* — il existe aussi un projet CodeursEnLiberte/confidentiel>. Le code source de [codeureusesenliberte.fr](https://codeureusesenliberte.fr) est dans le projet CodeursEnLiberte/www>.

N’hésitez pas à participer aux discussions et à suggérer des améliorations :)

**Ce projet contient:**

### Dans le [dépot](https://gitlab.com/CodeursEnLiberte/fondations/-/tree/main):

- [Nos statuts](https://gitlab.com/CodeursEnLiberte/fondations/-/blob/main/statuts.md)
- [Les décisions d’entreprise](https://gitlab.com/CodeursEnLiberte/fondations/-/tree/main/décisions%20d’entreprise)
- [Les comptes-rendus des assemblées générales et réunions](https://gitlab.com/CodeursEnLiberte/fondations/-/tree/main/comptes%20rendus)
- [Le logo de CeL](https://gitlab.com/CodeursEnLiberte/fondations/-/tree/main/assets)
- [Les slides de présentations faites à différentes occasions](https://gitlab.com/CodeursEnLiberte/fondations/-/tree/main/présentations)

### Sous forme d’[issues](https://gitlab.com/CodeursEnLiberte/fondations/-/issues/?sort=updated_desc&state=all&first_page_size=100):

Les discussions sur tous les sujets de la vie de la coopérative:
- ~Fonctionnement 
- ~Valeurs 
- ~"Nous rejoindre"
- ~Administratif 
- ~compta 
- ~Outils

### Dans le [wiki](https://gitlab.com/CodeursEnLiberte/fondations/-/wikis/home):

- Les guides de compta et d’administration, 
- le fonctionnement de coopérative,
- les étapes à suivre à l’arrivée d’un nouveau sociétaire, les guides de configuration des outils,
- etc.

