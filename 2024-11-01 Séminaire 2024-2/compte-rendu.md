
# Séminaire 2024-2

## Localisation

Rue de Rochepinard à saint avertin

[https://www.openstreetmap.org/way/198865733#map=19/47.369205/0.725143](https://www.openstreetmap.org/way/198865733#map=19/47.369205/0.725143)

En bus ca donne ca: [https://www.filbleu.fr/horaires-et-trajets/itineraire?tx\_tcnetwork\_journeys%5Baction%5D=map\&tx\_tcnetwork\_journeys%5Bcontroller%5D=Journey\&cHash=b557013f307a5ef0ab4861cd9513f61f](https://www.filbleu.fr/horaires-et-trajets/itineraire?tx\_tcnetwork\_journeys%5Baction%5D=map\&tx\_tcnetwork\_journeys%5Bcontroller%5D=Journey\&cHash=b557013f307a5ef0ab4861cd9513f61f)

## Propositions d’ateliers

### Interviews d’autres coopératives

Pour retrouver du sens dans notre activité commune je propose d’aller demander à d’autres coopératives qui font du développement informatique comment ils travaillent ensemble, ce qui fait qu’ils se sentent un groupe, ça pourra nous inspirer et questionner nos pratiques

### Formations à Codeurs en Liberté

On profite très peu actuellement de nos comptes CPF, ou même de notre budget propre pour nous former. J’avais évoqué au dernier séminaire l’idée de faire une formation premiers secours sur le temps du séminaire sans que je ne la mette en place.

J’aimerais pouvoir relancer cette idée, et peut être que l’on pourrait faire ensemble des formations pour se former sur des sujets informatiques ou autres.

### Montre moi sur quoi tu travailles

À chaque tour de table on se dit sur quoi on travaille, mais sans qu’il y ait de concret, je serai preneur d’un tour d’horizon des produits sur lesquels on travaille.

### Rôti mon travail !

Après que tu m’aies dit combien le produit sur lequel je travaille, je propose de rentrer un peu plus dans des détails d’implémentation ou d’architecture et que tu la questionnes.

### Roti sur mon travail

Mon travail est peut-être super. Mais est-ce que le temps que j'y consacre en vaut la peine ?  Il n'y aurait pas des solutions moins techniques pour arriver au même résultat et coûter moins cher à la société ? 

### Que faire en cas d'impayé ?

Qui porte la charge financière  ? Le collectif ? Læ codeureuses? Moit-moit ? 

### Projet de tuiles pmtiles pour les découpages administratifs

Quelqu'un nous a demandé si on pouvait mettre à dispo un export de [https://github.com/osm-without-borders/cosmogony](https://github.com/osm-without-borders/cosmogony) (qui sort une hierarchie administrative à partir des données OSM), ca pourrait etre chouette de faire ca sous forme de tuiles pmtiles, et de faire un site web associé.

### Prendre une décision pour le comptable

Ça serait chouette de profiter du séminaire pour prendre une décision sur le fait de se faire accompagner ou non par un cabinet comptable.

### Avis sur le temps commun mensuel

Est ce que ca fonctionne bien ? Est ce qu'on peut s'organiser mieux ?

### 

### Discussion Gis Geek

Pourquoi on entend des phrases du style "avec le WGS84, la précision est au mieux métrique". "Vous n'utilisez pas le Lambert 93 ? Pour nous c'est envisageable", etc J'aimerais bien creuser un peu plus le sujet.

### Jeux de société

Liste des jeux de société qu'on compte apporter :
  - quelqu'un·e  veut retenter un "race for the galaxy" ? (Francis) ouiii
  - Akropolis (proposé par Thimmy)
  - Courtisans (Francis)
  - scout
  - cartes
  - that's not a hat

### Ajustements sur le site web

[https://codeureusesenliberte.fr/entreprise/ethique/charte](https://codeureusesenliberte.fr/entreprise/ethique/charte) il y des liens morts

### Subvention BPI zSMS

[https://www.iledefrance.fr/aides-et-appels-a-projets/innovup](https://www.iledefrance.fr/aides-et-appels-a-projets/innovup)

### Positionnement produits CeL

- cocarto
- zSMS
- les prochains

# Compte-rendu

## Atelier "Montre-moi sur quoi tu travailles" / "Rôti mon travail"

### Vincent : indicateurs de l'ANCT

Vincent présente son travail de présentation des indicateurs de l'ANCT.

Avant : chaque territoire envoie des fichiers Excels ; des petites mains compilent.

Après : les données sont importées sur un site, puis présentées automatiquement en HTML ou PDF.

L'import se fait avec des descripteurs au format YML qui décrivent comment importer un Excel spécifique. La présentation est faite en MDX (Markdown + JSX). Possibilité de comparer les indicateurs entre différents territoires. API GraphQL pour le requêtage et l'export. Le code et la config YML sont dans deux repos séparés.

Travail à mi-temps. Doutes sur le fait que pas mal d'équipes font la même chose dans l'État : est-ce que y'aurait pas moyen d'avoir une usine à dashboards communs ? En même temps, Métabase expose trop de complexité aux utilisateurs.

Une généralisation de ça pourrait être une idée de produit pour Codereuses en Liberté ? La plupart des administrations veulent un dashboard alimenté par des données open-source. Mais en payant, va savoir ; y'a sans doute pas de thune. Alors qu'il y en a plus en allant déployer Grist.

Roti : l'équipe est vraiment cool (trois devs, une designeuse). 

### Thimy : bump.sh

Générateur de documentation d'API. On fournit un fichier de spec, et on récupère une documentation jolie est navigable. Gère OpenAPI et AsyncAPI (et un jour GraphQL, Soap, etc). C'est comme SwaggerUI, APIDoc, etc - juste on le fait à notre façon, avec une attention à l'UI. Également de la génération de changelog, fait à partir des différences quand on pousse une nouvelle version de la documentation. Travail actuel : possibilité d'envoyer des requêtes directement depuis l'interface web.

Stack : StimulusJS, 

Hub de gestion pour les clients : regroupement d'APIs, déploiements, gestion de droits fines, pour des APIs privées, etc. Pas encore de commercial dans l'équipe, mais c'est en cours.

### Francis RnB : 

Référentiel National des Bâtiments, financé par l'IGN et le CSTB. Le  but est de données un ID à chaque batiment de france. Pas vocation à avoir plus d'info sur les bâtiments, mais d'avoir un ID pivot entre les différentes bases.

[https://rnb.beta.gouv.fr/stats](https://rnb.beta.gouv.fr/stats) << ~43M de batiments référencés.

Pour le moment 6 jeux de données intégrent l'ID :
  *  le CSTB (tout plein d'info sur des batimetns, leur DPE, l'années de construction, ...), 
  * équipement sportif
  * BD TOPO de l'IGN ,
  * ...

Une jolie carte pour explorer les données, on peut voir tous les bâtiments, leurs addresses, et des infos. Ajout d'un formulaire pour pouvoir signaler des erreurs.

Pas encore d'interactions avec les impôts alors qu'ils ont des infos super intéressantes pour savoir quand un bâtiment est vraiment construit.

Jeu de l'été du RNB, pour récolter des signalements, beaucoup trop de signalement à gérer (8500 signalements), dont une nana qui a posé 1 semaine de congé et a fait 4500 signalements.

Partie tech: promesse de pérénité des ID RNB, un ID ne doit jamais disparaitre. Tout est historisé dans la BDD, grace à [https://wiki.postgresql.org/wiki/Temporal\_Extensions](https://wiki.postgresql.org/wiki/Temporal\_Extensions). Quand une ligne bouge dans la BDD, un trigger enregistre l'ancienne version dans une table d'historique.  Dans l'historique, on peut aussi remonter les fusions / découpages de bâtiments.

Donne des accès à des équipes externes pour écrire dans la bdd. Réfléchissent à mettre des gardes fous pour limiter les boulettes.

Equipe de 3 dev (1 junior qui fait du react, 2 back), 3 chargés de déploiement, 2 intra.

## Pico 8 sur Game Boy Color

mardi après-midi

Présentation de Pierre qui travaille depuis plusieurs mois sur la création du portage d'une animation de Pico 8 (émulateur de console) sur GameBoy Color. Le problème vient de la façon de gérer les couleurs de la GBC, qui, si elle gère 65000 couleurs (contre 16 pour la Pico8), ne peut gérer, à un instant T, que 8 palettes de 4 couleurs, et chaque bloc de 8x8 pixels référence une palette, et donc ne peut avoir que 4 couleurs différentes).. Et l'animation de Pico 8 a besoin de plus que 4 couleurs différentes sur un bloc.

La feinte pour réussir ca est de faire le rendu d'une ligne, changer les palettes super vite, et faire le rendu de la ligne suivante.

Le code est fait en assembleur.

Présentation de différents codes assembleur pour changer les couleurs  des palettes le plus vite possible, pour avoir le droit d'écrire toutes les couleurs des palettes avant le rendu de la première ligne.

## Responsabilité des impayés

mercredi matin

Qui porte la responsabilité des impayés ? La coopérative ou les membres ayant effectués la mission.

Francis: j'ai l'impression qu'un ratio de 50% est un bon moyen d'avoir de la solidarité entre nous mais de la responsabilisation à aller chercher ses impayés.

Thomas: est ce qu'on pourrait plafonner ?

Francis: la RC pro nous protégeait contre ca, est-ce qu'on la reprend ?

Pierre: il faut qu'on ai bien la recherche des impayés en tête lors de la compta

Vincent: C'est pas juste de décider d'un truc rétroactif, il faut que ce qu'on discute s'applique sur l'avenir. C'est important qu'en tant que collectif on se protège contre ca.

Francis: Il faudrait qu'on fasse un calcul pour savoir le max des impayés qu'on pourrait avoir en étant de bonne fois.

Pierre: est-ce que dire que la coopérative prend en charge jusqu'à 2 mois d'impayés. (le délai pour savoir qu'il y a un souci de paiement).

Francis: je trouve ca cool qu'il y ait une part de charge personnelle là-dessus, un indépendant doit aller chercher ses impayés.

Thomas: la RCPro pourrait nous protéger à pas cher là-dessus non ? Après, on ne connait pas les modalités de la protection

Thomas: Ca me rassurerait qu'on mette une limite par personne

Francis: est ce qu'il ne faudrait pas prendre une décision collective, par client, quand nos impayés d'un client dépasse une limite (+ de 6 mois ou > 50 k€) ? 

Antoine: j'aime bien les règles, simple, j'ai l'impression que les 50% de responsabilité + le suivi fin des impayés devraient suffire.

Conclusion: 

On se dit que pour les impayés actuels, la coopérative prend en charge, mais pour la suite, on se dit que la responsabilité est partagée à 50/50 entre les personnes ayant fait la mission et la coopérative.

## Se faire aider par un comptable

mercredi matin

Il y aura une nouvelle logistique à mettre en place avec les outils d'Endrix mais ça nous fait pas trop peur.

On se dit que c'est chouette d'avoir des services ad-hoc à la carte pour être dans les clous côté RH/social.

Pierre: note, on ne peut pas garde zefyr sans la compta, il faut migrer tout.

Antoine : On pourrait continuer à faire comme avant mais passer ver

Francis : En cas de nouveau contrôle, est-ce qu'on pense qu'Endrix sera autonome ? On sera surement impliqués pour éclaircir sur nos spécificités.

Conclusion : On se dit qu'on dit ok avec Endrix, on a pas d'alternatives creusée, et on se dit que c'est chouette d'avoir de l'aide et des gens qui s'y connaissent, et le prix ne va pas être très différent du prix de Zefyr + audit comptable.

On se dit qu'on prend comptable + RH. Incertitude sur comment ca se passerait si ca se passe mal, mais YOLO.

Note: dans le mail, il faut aussi leur demander comment se passerait la partie intéressement.

## Avis sur le temps commun mensuel

mercredi après-midi

### Temps commun

Thomas: pas fait beaucoup, mais trouvé ca chouette d'échanger. Le manque de préparation n'est pas forcément un problème.

Francis: jour en commun, j'y es été qu'à une, mais j'ai bien aimé la partie jeux de société, c'est top de prendre du temps ensemble. Pas beaucoup de préparation sur la partie temps de coopérative, et sentiment de flottement.

Vincent : zappe toujours la journée, manque de préparation.

Antoine : J'ai  l'impression qu'il y a un manque de motivation pour préparer en amont , du coup il y a du flottement, mais on fait quand même des trucs utiles, et ça nous permet de discuter de sujet de fond plus que 2 fois par ans.

Conclusion : globalement les gens sont contents. Faut faire plus de rappels avant pour motiver les gens à remplir le pad avant.

### Tourniquet

Vincent: du mal à s'impliquer avec le tourniquet.

Thomas: Le tourniquet, c'est la partie facile, mais c'est chouette d'etre plus impliqué. Le coût de synchro est peut etre un peu grand, c'est de temps en temps plus simple de faire ca tout seul.

Francis: Très content du tourniquet. Globalement, c'est mieux qu'avant.

Actions: on fait des groupes de 2 personnes, et on communique bien sur mattermost et gitlab sur l'équipe désignée.

## Conseil

Pas de notes, fallait être là

## Formations

Vincent: j’aimerais bien qu’on fasse des formations en interne. Ça serait aussi le moyen de faire des temps collectifs

Antoine: on pourrait organiser une session avec [https://groupe-egae.fr/](https://groupe-egae.fr/)

Vincent: on peut visionner ensemble une video et mettre en pause

Nicolas: j’ai bien aimé la présentation de Pierre sur la game boy, et j’aimerais bien revoir la présentation de Francis sur PG. 

Antoine: j’ai trouvé intéressantes les présentations sur les métiers de chacun mais j'ai peur que quelqu'un se motive à préparer une présentation et que ça n’intéresse pas les autres.

### Sujets d’envie:

* jujutsu
* css moderne
* PG avancé

## Mise à jour de www

- Page d’accueil
  - Codeurs → codeureuses
  - Pavés « Hero » / « Nos réalisations » / « Nos domaines de compétence » / « Nos clients » / « Derniers articles de blogs » / « À propos » 
- Idées :
  - Comment la tenir à jour ?
- /blog :
  - marge entre les articles
  - liens cassés
  - images cassées
  - images qui débordent
- Qui sommes nous :
  - summary/details sur les ancien·nes membres
  - Lien vers la charte éthique
