---
slideOptions:
  allottedMinutes: 45
  theme: blood
  transition: slide
---

# 👋

<p>
<img src="https://www.codeursenliberte.fr/logo.svg" alt="" width="80" class="plain" style="background:none;">
Codeurs en Liberté
 
<img src="https://42l.fr/assets/svg/logo_42l.svg" alt="" width="80" class="plain" style="background:none;">
42l
</p>

<p>Jeudi 27 février 2020 - 14:00</p>

----

## Éthique du métier de développeur

<p>
<img src="https://www.codeursenliberte.fr/logo.svg" alt="" width="80" class="plain" style="background:none;">
Codeurs en Liberté
 
<img src="https://42l.fr/assets/svg/logo_42l.svg" alt="" width="80" class="plain" style="background:none;">
42l
</p>

<p>Jeudi 27 février 2020 - 14:00</p>

Note:
Préintro pour nous présenter:
- développeurs mobile et web,
- agences, différents trucs
- capitaine train
- après CT, truc ensemble
- ça nous a amener à réfléchir à notre métier
- c’est ce qu’on vient vous raconter

---

Note:

Intro:

Il y  deux trucs dont on voudrait parler: la structure du travail, et les implications morales.
Le métier de développeur est plein de clichés, entre autre sur l’environnement de travail; certains sont assez réalistes, et parfois même caricaturaux. Cela dit, les développeureuses ne sont pas uniquement attirés par des salaires et des technos cool, mais aussi par intérêt pour des projets utiles. Dans nos parcours respectifs, on peut un peu parler de prise de conscience sur plusieurs années.

Plan: 

- différents types de structures
- vie d’indépendant
- modèle de coopérative
- limites des petites structures

- sens, finalité du code
- conséquences et responsabilités 
- syndicalisme, onestla.tech, engagement politique
- logiciel libre

---

## Les développeurs
dans leur milieu naturel


Note:
2 clichés d’entreprises informatiques

---

👔 la SSII de l’angoisse 👔

Note:
la “société de service en informatique”
qui emploie des développeurs à la pelle et les envoie en tant que consultants dans les banques, les grosses boites…

----

![](https://i.imgur.com/fHMiryW.jpg)

Note:
C’est le monde des photos stock. 

----

🌈 la starteupe cool 🌈

Note:
avec des babyfoot, du café à volonté, des bureaux dans le sentier, et des levées de fond.

----

![](https://i.imgur.com/MK46ood.jpg)

Note:
C’est aussi le monde des photos stock.

----

Note:
En réalité, les deux modèles ne sont pas si éloignés, et dans la vraie vie il y a plein d’openspaces un peu bondés.

----

![](https://i.imgur.com/NE20vx0.jpg)

Note:
Par ailleurs, ni les “SSII” ni les “startups” ne sont des secteurs économiques.
Ça ne veut pas dire grand chose, “travailler dans l’informatique”.
En fait tous les secteurs ont besoin d’informaticiens.

L’automobile ou de la finance recrutent plus d’informaticiens qu’autre chose.
Dans ces cas c’est massivement via des SSII.

Mais les startup non plus ne sont pas un domaine d’activité, “la tech” ça ne veut rien dire.
On trouve des startup dans tous les domaines: biotech, fintech, legaltech, edtech, greentech…

---

## 🏢

Note: LES GROSSES BOITES

----

## Les grosses boites c’est nul


Note:
Anecdote perso sur les consultants:
Ma première (et ma seule) expérience de consultant: 
- 1 semaine, placé (via une magouille) par une petite boite chez Hachette Groupe
- des consultants d’Accenture travaillait depuis six mois sur
- “rationalisation des processus de publication entre les différentes marques, du manuscrit à la PLV”
- ils n’avaient jamais fait le moindre schéma d’interface, 
- n’avaient pas la moindre idée des décisions que devraient prendre leurs utilisateurs.
Leçons retenues:

----

### 🚪 Virez tous Les consultants

Note:
Les consultants externes ont comme objectif d’être payés, pas d’améliorer réellement la situation, le projet.
Leur allégeance va à leur employeur (ici Accenture), pas à leur client.
Je force un peu le trait, mais ce n’est pas complètement une caricature: ils rendent des comptes à leur employeur, pas vraiment au client.
C’est réellement la conclusion que j’en tirais après coup.

----

### 🔥 Brulez vos chefs

Note:
Les rapports de hiérarchie empêchent l’honnêteté intellectuelle
La hiérarchie, c’est “qui donne les ordres”. Le client ou le supérieur ont la même relation.
Les consultants d’Accenture et les salariés d’Hachette qui géraient le projet ne voulaient pas admettre à leur directeur que le projet n’allait nulle part.
- parce que ça les arrange financièrement.
- mais aussi parce que ce n’était pas leur rôle. Chacun reste à sa place.

----

### ✊ Le KPI est ton ami

<em class="fragment">(non)</em>

Note:
Ce projet n’était pas stupide en lui-même, 
ça avait probablement un sens de vouloir utiliser des outils communs pour tout le groupe.
Mais la mesure du succès du projet se mesurant en “ETP économisé”.
On tablait sur un ou deux salariés en moins.

À l’époque, ma réflexion s’arrêtait sur la stupidité économique du projet.

En réalité, les objectifs étaient beaucoup plus flous: 
On améliorerait le travail de dizaines de personnes, mais de façon difficile à quantifier.


---

## ‽
<em class="fragment">
TODO: trouver un meilleur titre pour cette slide
</em>

Note: (c’est une blague)
En réalité, tout ça me mettait mal à l’aise vis à vis de mon métier, sans que je sache réellement quoi en faire.

Un point à peu près concret: dans les grosses organisations, 
sociétés privées comme administrations publiques,
il est très difficile de se remettre en question et trouver un réel objectif à son travail.

----

### 🧍🧍🧍🧍🧍🧍🧍

Note:

Au delà de 50 personnes dans une structure, on a des rivalités entre équipes: 
Il y a des intérêts divergents, par exemple entre 
- le service marketing, 
- la sécurité informatique.
Les petites structures ont l’avantage de la meilleur transmission des savoirs en interne:
tout le monde est au courant de tout, ou au moins sait ce sur quoi travaille chacun.

-> https://blog.sugoi.be/quatre-ans-avec-atos.html

---

---

## 💻☕️🏓✨

Note: Le petit monde merveilleux des startups
Tout n’est pas forcément plus rose.

----

### 🐜🐜🐜🐜🐜🐜🐜🐜

Note:
Il n’y a pas de protection sociale dans les petites boites. 
Les semaines de 50, 60 heures, ça existe.
Le monde du jeu vidéo, c’est ça.

Les grosses boites ont des “délégué du personnel”, des “syndicats”, et autres trucs un peu désuets.
Mais en réalité, on ne les a pas inventés par hasard.

Les patrons qui incitent à se défoncer pour le projet.
Derrière le côté cool, de nombreux développeurs oublient que ce n’est pas _leur projet_.

----

## 💸

Note:
Une bonne partie des startups n’a pas d’objectif réel de rentabilité, seulement de rachat.
Et une bonne partie des startups tech qui gagnent vraiment de l’argent sont ceux qui vendent aux autres startup:
- Les vendeurs de pelles sont les seuls gagnants des ruées vers l’or.
- C’est le concept même de bulle.
Accepter un investisseur, c’est accepter que le destin de la boite est d’être revendue, pas de construire et de commercialiser un produit.

----

## 🤪

Note: Vacuité des projets
Ça amène aussi à de nombreux projets fondamentalement débiles.
- énième “réseau social géolocalisé pour les entreprises”
- 90% des chatbots
- 99% des trucs autour de la blockchain

Ici aussi, si le seul indicateur de succès, le KPI, c’est _l’exit strategy_,
alors le but n’est pas vraiment de construire un produit utile,
mais de rendre la structure achetable par un plus gros groupe.

---

---

## 

![](https://i.imgur.com/3cogCnh.png)

Note: ce que veulent les développeurs


Note:
Deux traits distincts, avantages de l’autonomie:
- épanouissement personnel
- utilité sociale
Travailler moins, plutôt que se défoncer 50+ h par semaine pour le projet de quelqu’un d’autre.
(on ne vit pas de sa passion)

Ne pas participer à la société de surveillance:
  Témoignage d’un dev embedded qui ne trouve que des jobs non-éthiques.

-> https://iosdevsurvey.com/2019/02-your-career/#q23
-> https://lwn.net/Articles/808489/



---

## 💃
_Fig. 1 - développeur indépendant_


Note: L’indépendance, aka la liberté
Cela dit, c’est pas évident.
- Isolement 
- Paperasse
- Risque en cas de coup dur
- Trouver des clients reste relativement facile dans notre métier, mais ça reste une angoisse.
- On peut se retrouver dans une position de hiérarchie vis-à-vis du client

----

## 👯‍♀️
_Fig.2 coopérative_

Note:
Une coopérative c’est quoi?
- 1844 tisserands de Rochdale, d’abord une association d’ouvriers pour améliorer leurs conditions
- qui voulaient ne pas dépendre des commerçants,
- mais aussi garantir des prix de vente
- 1895 https://fr.wikipedia.org/wiki/Déclaration_sur_l'identité_coopérative
C’est une façon de voir l’activité économique, un type de gouvernance.

----

### Principes coopératifs

<p class="fragment">Adhésion volontaire et ouverte à tous.</p>
<p class="fragment">Pouvoir démocratique exercé par les membres. </p>
<p class="fragment">Participation économique des membres.</p>
<p class="fragment">Autonomie et indépendance.</p>
<p class="fragment">Éducation, formation et information.</p>
<p class="fragment">Coopération entre les coopératives.</p>

Note:
Pour moi, les principaux points sont la démocratie et la participation économique. Le reste en découle.

----

<p class="fragment">SCOP</p>
<p class="fragment">SCIC</p>
<p class="fragment">CAE</p>

<p class="fragment">CGSCOP</p>
<p class="fragment">Groupement national de la coopération</p>
<p class="fragment">Alliance coopérative internationale</p>

Note:
En France, il y a trois formes principales du point de vue administrative, même si dans l’absolu n’importe quel société peut se déclarer coopérative.
Les SCOP sont des coop de production,
Les SCIC des partenariats public/producteur/consommateur
Les CAE du portage salarial
(Ce sont plutôt les SCOP et les CAE qui vous intéressent)

La déclaration de 1895 a donné naissance a des institutions structurant au niveau national et international.
L’ACI par exemple représente le mouvement coop à l’ONU et gère le tld .coop.

----

![](https://i.imgur.com/JsuqkLG.jpg)

Note:
La démocratie est le principe de base, la façon précise de gérer une coopérative est libre.
On peut choisir un système selon ses propres objectifs.
Par exemple pour CeL, le but était d’avoir à la le confort des indépendants et la sécurité financière du salariat.
Ça se traduit dans des règles spécifiques sur la façon dont nous nous rémunérons et la façon dont nous comptons nos heures.
Chaque coopérative peut choisir sa gouvernance, son mode de récrutement, son mode de rémunération, d’autres font différemment.

----

## ‽

Note:
C’est très bien, on a un environnement de travail super, on est heureux mais
Tout cela est “facile” parce que les développeurs sont, aujourd’hui en tout cas, privilégiés.
Il n’y a virtuellement pas de chomage, et les salaires sont élevées: 
on peut prendre ce genre de risque, on peut se mettre à temps partiel…
On peut aussi dire que c’est un avantage qu’on se doit de saisir, ne serait-ce que pour montrer l’exemple.


---

Note: Transition
On a parlé jusque là d’autonomie, de démocratie au travail, d’épanouissement.
Un autre point qu’on a mentionné était que les développeurs cherchent souvent à donner un sens à leur travail, une utilité sociale.

Au fur et à mesure de nos carrières, on a pris conscience des implications de notre métier sur la société; on a aussi réalisé que le concept d’éthique était, d’un côté, ignoré par beaucoup, et en même temps que nous n’étions loin d’être les seuls ou les premiers à réfléchir à ces sujets. 

---

## Éthique et informatique

Note:
Écrire du code, c’est bien, mais à un moment, il est executé.
Il va avoir des conséquences, sur des personnes.

---

### Le code a un but.

<!-- .slide: data-background="#99241E" -->

Note:
Je ne veux pas tenir un discours anti-technologiste, 
L'informatique n'est pas en elle-même bonne ou mauvaise: 
On écrit des programmes pour resoudre des problemes spécifiques, l’informatique est un outil.

Par contre, les programmes, eux-mêmes, ont un but, et ce but n’est pas nécessairement bienveillant.
Il peut être en lui-même malveillant, 
ou il peut être utilisé par des personnes malveillantes.

---

## L´éthique c'est quoi?

Note:
En philosophie, c’est la discipline qui étudie les valeurs et les finalités.
Chacun a sa définition du bien et du mal,
la déclaration des droits de l’homme est un bonne plateforme.

----

![](https://i.imgur.com/xvJ2UGQ.jpg)

Note:
En philosophie, c’est la discipline qui étudie les valeurs et les finalités.
Chacun a sa définition du bien et du mal,
la déclaration des droits de l’homme est un bonne plateforme.

----

## L’éthique c'est quoi?

<p class="fragment">Un code de valeurs morales</p>

Note:
En philosophie, c’est la discipline qui étudie les valeurs et les finalités.
Chacun a sa définition du bien et du mal,
la déclaration des droits de l’homme est un bonne plateforme.

---

Note:
Revenons au code.
Quand on écrit du code, c’est pour un but, pour résoudre un problème.

Ce problème est le plus souvent celui de quelqu’un d’autre.
On écrit un programme dont on ne va pas être l’utilisateur, ou le bénéficiaire.

---

### Empathie 

<p class="fragment">🧜</p>


Note:
Le developpement logiciel demande autant d’empathie que de connaissances techniques.
Contrairement au cliché, le métier de développeur demande de parler aux gens.

---

---

### « Informatique »

![](https://i.imgur.com/P8eEUag.jpg)

Note:
Autre point historique:
Le terme “informatique” a été créé, choisi, en 1962. (Philippe Dreyfus, futur directeur de Capgemini)
pas apparu comme un jargon.
Terme choisi par de Gaulle, qui l’a préféré à “ordinatique”.
“technique de l’information”, plutôt que “science des ordinateurs”
Si l’on se concentre sur l’étymologie, l’informatique est plus proche de l’anthropologie que des mathématiques.

Ce n’est pas un point de vue partagé par tout le monde


----

> Computer science is known as "informatics" in French, German, and several other languages, but American researchers have been reluctant to embrace that term because it seems to place undue emphasis on the **stuff** that computers manipulate rather than on the **processes** of manipulation themselves. 
> -- Donald Knuth

Note:
Pour Knuth, les ordinateurs manipulent des données abstraites.
C’est cohérent avec le titre de “The Art of Computer Programming”

---

## 🚎

Note:
Les questionnements sur les conséquences de l’informatique sur la société ne sont pas récents.
1978, rapport Nora à la demande de VGE.

----

<video controls src="https://mp4.ina.fr/lecture/lire/site/embed:1/securekey/2I9-URegA8Vio330cZh3znuRB92Jf1-z9niSXifncJENHX83g4t4EaPq99hzyJmDM4bHCC_01hW6a5lClNnBmH-Ja4hF2DIkh6WAEmtGt40.mp4/CAA7800628501::Informatique%20%3A%20rapport%20Nora"></video>

Note:
La même année, loi informatique et libertés.
Les points soulevés en 1978 sont encore pertinents.
40 ans plus tard, on peut refaire un bilan des conséquences de “l’informatisation de la société”.
Comme on le disait au début, l’informatique touche tous les domaines.

> https://www.ina.fr/video/CAA7800628501/informatique-rapport-nora-video.html

----

### Ubérisation 🚕🚴🍟

Note:
Née “Sharing economy”, devenue ubérisation entretemps.
Uber continue de perdre 1 milliard par trimestre,
La principale innovation consiste à lever énormément d’argent pour pouvoir contourner des réglementations
sur le droit du travail ou sur les transports.

> https://oversharing.substack.com

----

### Vos choix tuent des gens

Note:

Uber qui désactive les sécurité dans les voiture autonomes, et déclenche des accidents;
Le dieselgate, 
(Les VW étaient programmées pour tourner à moindre puissnce pendant les tests de pollution.)
ce qui va entrainer plus de pollution, donc plus de maladies.
Ou plus subtil: une modification sur le site de pole-emploi qui rendrait l’inscription plus complexe,
plus de gens perdent leurs droits, plus de suicides.

----

### Discrimination

Note:
Algo IA racistes, parce qu’entrainés uniquement avec des photos de blancs.
L’algo de recrutement d’amazon, qui reproduit les biais des recruteurs humains, 
et qui désavantage les femmes.
Besoin de diversité dans les équipe.

----

### Surveillance de masse

Note:
Cas de conscience concrets en informatique:
- ICE / github
- IBM pendant WW2: collaboration de la filliale allemande avec le régime nazi, camps d’internement des japonais aux US.

Décret cette semaine pour un énième fichier de la police, permetttant de recenser à peu près tout sur n’importe qui.
Certains policiers ne comprennent pas l’opposition, parce qu’ils notent déjà ces informations chacun de leur côté.
C’est pourtant l’idée à la base de la CNIL, la loi I&L est due à la (non) création d’un fichier du même style le fichier SAFARI en 1974.

----

### Vie privée

Note:
Sur la réglementation: les développeurs sont les mieux placés à la fois pour comprendre les implications faire appliquer le RGPD.

Sur tous les sites, même chez les petits acteurs, le tracking total est devenu un réflèxe,
On met GA systématiquement.
google, netflix, amazon, etc ont des profils extremement détaillés sur leurs utilisateurs, et monétisent ces profils.
C’est le capitalisme de surveillance.

----

### Illectronisme

Note:
Un sujet plus général, plus lié à l’informatisation de la société elle-même.
Difficultés d’accès liées à la dématérialisation
Pas uniquement le service public.
C’est une forme de discrimination.
Pas uniquement les vieux: par exemple, un service “nouveau” qui n’existe que sur app mobile moderne.

----

### Opacité

Note:
Du code qui prend des décisions pour des gens doit être public
Parcoursup, par exemple.
L’algo qui déclenche les contrôles et les radiations de Pôle Emploi.

----

### Dark patterns

Note:
Tout cela n’est pas abstrait, on vous demandera à un moment d’implémenter des algorithmes ou des interfaces manipulant les utilisateurs:
l’exemple le plus commun est la case à cocher “inscrivez-moi aux mailing lists de vos partenaires” juste en dessous de la case “accepter les CGU”.

---

---

## Re-responsabilisation ✋

Note:
J’avais tendance jusq’à récemment de dire que les développeurs étaient “déresponsabilisés” mais ces derniers mois me donnent l’impression d’une prise de conscience.
Ne serait-ce que parce qu’on est invités à 42 pour en parler.
Sujet qui devient fréquent dans les confs.

-> https://speakerdeck.com/ruanbrandao/software-ethics

----

### Charte d’éthique et code de déontologie 

<p class="fragment">https://ethics.acm.org</p>
<p class="fragment">IEEE - IEEE Code of Ethics</p>
<p class="fragment">IA</p>


Note:
On ne part pas de nulle part: il existe déjà des chartes, 
similaire à ce qui existe dans d’autres domaines: médecine, ingéniérie civile.
- Association for Computing machinery ACM (révisé en 2018), 
- IEEE
- Tout un pan de recherche sur la “roboéthique” et l’IA

La question d’une charte est toujours celle de l’appliquabilité;
une charte ne s’applique qu’à ceux qui la signent.
Le concept de corporation est largement oublié aujourd’hui.
Par ailleurs, la déontologie est souvent plus associée “professionalisme” en lui-même.

On en voit une apparaitre tous les 3 mois, on a fait la nôtre chez Codeurs en Liberté.

---

---

## Et le logiciel libre là-dedans?

> Le mouvement du logiciel libre ne s’est jamais positionné clairement contre la surveillance. -- @lunar

Note:
Le libre, au moins, force la transparence. 
C’est necessaire, mais pas suffisant, pour empecher, par exemple, la fraude.
Le mouvement libre se conçoit à l’intérieur de la révolution informatique, 
mais ne prend pas partie sur le fond.

> https://mastodon.potager.org/@lunar/103393488397383465
> https://informatique-ou-libertes.fr

----

### Le web moderne est centralisé

Note:
Tout le monde aime le libre, tout le monde fait de l’opensource, mais
on crée des sites sur lesquels les utilisateurs s’enregistrent
y compris dans les modèles de startups:
Pas des outils qui peuvent être réellement contrôlés par leurs utilisateur.
-> pas de liberté des utilisateurs sans accès aux données
-> peut-on même faire un site web réellement libre en 2020?
Même framadate, par exemple, est dépendant du bon vouloir de framasoft.

----

### Liberté 0 de la GPL

>  La liberté d’exécuter le logiciel, pour n’importe quel usage

Note:
L’accès complet aux données est la même liberté que celle à à hacker son logiciel
- encore faut-il connaître les algorithmes exploitant les données
- rgpd et gpl, même combat

----

### Code of conducts

Note:
En quelques années, c’est devenu normal pour les confs, voire anormal de ne pas en avoir.
Idem sur les projets opensource: il y en a dans les templates de projets de github.
Plus largement, c’est le signe d’une prise de conscience.
Le but est la lutte contre la discrimination,
À partir du moment où on remet en question nos pratiques en tant que communauté,
on réfléchit aussi au sens de notre action.


-> https://opensource.guide/code-of-conduct/

----

### Licences éthiques

<p class="fragment">ethicalsource.dev</p>
<p class="fragment">996.icu</p>
<p class="fragment">www.open-austin.org/atmosphere-license</p>

Note:
Empêcher des usages est contraire à la liberté 0 de la GPL.

- Empêcher des réutilisations contraires aux droits humains : hippocratic licence
- Respecter la santé des travailleurs. 996 signifie 9h - 21h, 6 jours par semaine
- Lutter contre la crise de l’environnement:

> sur l’applicabilité de la hippocratic licence v2: https://twitter.com/CoralineAda/status/1228391919295836160 
> - firstdonoharm.dev https://firstdonoharm.dev/faq.html

---

Note: Transition
Libre ou non, il se passe des choses positives par endroits, entre prise de conscience et rébellion.

---

### Projets engagés

<p class="fragment">“Civic tech”</p>
<p class="fragment">“Code for good”</p>

Note:
Betagouv et autres trucs étatiques:
C’est dépendant de l’administration, et souvent prompt à être récupéré par l’action politique
Code for good, c’est un peu du “ethical washing”, des hackathons “éthiques” financés par des grosses boites.
Je vois quand même ça comme une reconnaissance d’un mouvement de fond.

----

### Décentralisation

<p class="fragment">Mastodon</p>
<p class="fragment">Mobilizon</p>
<p class="fragment">Nextcloud</p>
<p class="fragment">Tor</p>
<p class="fragment">Framasoft</p>

Note:
> framatrucs / dégooglisons / déframatisons

----

### Engagement

<p class="fragment">(politique)</p>

Note:
Engagement syndical et politique
- STJV
- greve.cool et onestla.tech
US: https://collectiveactions.tech

---

Note: (respire)

---

## 🌅

Note: 
La question du début aurait pu être “Les développeurs sont-ils des mercenaires?”
Les dev sont ils de la chair à coder?
C’est ironique de dire ça ici, j’ai pu avoir des propos assez durs sur une école du même style à l’autre bout de paris.
Clairement non. En fait, la plupart des dev à déjà tendance à remettre en question son travail.
En cherchant des stats sur les écoles, on trouve des articles sur 42 plus que sur les autres écoles.
Manifestement, 42 est perçue différemment.
Ce qu’il manque, c’est plus la capacité à s’organiser pour ne *pas* agir contre ses propres valeurs.
Une des pistes, c’est donc de se rassembler en coopératives plutôt qu’indépendents seuls.

> Gens:
> [Coralina Ada Ehmke](https://where.coraline.codes)
> [Ruan Brandao](https://twitter.com/RuanBrandao)
> [Informatique ou libertés – Lunar](https://informatique-ou-libertes.fr)