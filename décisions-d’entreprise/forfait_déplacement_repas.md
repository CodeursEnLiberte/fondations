# Nouvelle formule du forfait déplacement, télétravail, repas

## Historique

Codeurs en Liberté versait un forfait de 100€/mois destinés à couvrir forfaitairement les frais de déplacement (vélo, abonnement transport en communs), télétravail et repas.

## Contexte

Suite à un contrôle URSSAF, il s’avère qu’il n’est pas possible de donner un forfait pour les repas.

En ce qui concerne les déplacements et télétravail le ratio doit être défini explicitement.

## Nouveau calcul

Codeurs en Liberté ne rembourse plus forfairement le repas. Des notes de frais au cas par cas doivent être faites lorsque læ salarié·e est en déplacement.

### Déplacements

Sachant qu’une année consiste en 218 jours travaillés par an au forfait jour, selon la convention collective Syntec [^1].

Au choix du salarié, un forfait est versé :
- 566,80 € de forfait de mobilité durable [^2]
- remboursement des abonnement de transport. Au minimum 50%, au maximum 75% ou 566,80€ (valeur la plus faible)
- 2,60 € par jour de télétravail [^3]
- 0 € pour un déplacement motorisé individuel (voiture, moto…)

Le montant est calculé au prorata du nombre de jours de chaque mode de travail ou de déplacement effectivement pratiqué par læ salarié·e et est annualisé en comptant 218 jours de travail par an.

Concrètement, quelque soit le nombre de jours chez le client ou en télétravail et si le mode de transport est mobilité active ou transport en communs, le forfait annualisé est de **46,40 €/mois**. Pour chaque jour où le trajet est effectué en voiture ou en moto, il faudra déduire 2,60€. 

### Repas

Le lieu de travail habituel de læ salarié·e est à son domicile.

Lorsque læ salarié·e n’est pas en télé-travail, des notes de frais pour _petit déplacement_ peuvent être établies. Le montant maximum du remboursement est de 20,20€ [^4].

## Date d’application

À partir d’aout 2023.

## Références

- [^1] https://www.syntec.fr/wp-content/uploads/2022/04/avenant-n1-du-01-04-2014-duree-du-travail.pdf
- [^2] https://www.service-public.fr/particuliers/actualites/A14046
- [^3] https://www.urssaf.fr/portail/home/employeur/calculer-les-cotisations/les-elements-a-prendre-en-compte/les-frais-professionnels/le-teletravail.html
- [^4] https://www.urssaf.fr/portail/home/taux-et-baremes/frais-professionnels/indemnite-de-petit-deplacement/repas.html
