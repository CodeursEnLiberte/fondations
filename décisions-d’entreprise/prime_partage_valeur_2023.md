# Prime de partage de la valeur

## Historique

La prime de partage de la valeur[^1] avait été mise en place chez Codeurs en Liberté lorsqu’elle était surnommée « Prime Macron ». À cause de ce nom, certaines personnes ont choisi de la refuser.

Lors d’un contrôle URSSAF, il nous a été reproché de ne pas avoir eu de trace écrite détaillant ce refus, amenant à une forte régularisation.

## Mise en place systématique

Pour éviter un futur redressement, nous avons décidé lors du séminaire d’automne à Tours de verser systématiquement la prime.

La mise en place de la prime peut être décidée :

- unilatéralement de l’employeur.
- par un accord d’entreprise.

Même si la décision a été approuvée par 100% des salarié·e·s, nous nous plaçons dans le cadre d’une décision unilatérale de l’employeur.

## Montant

Codeurs en Liberté étant une entreprise de moins de 50 personnes, ayant mis en place un accord de participation, le montant maximum de la prime est de 6000 €.

Nous décidons de verser 6000 € par personne.

## Cotisations sociales

La prime sera versée sur le salaire d’octobre 2023.

Les salarié·e·s dont les revenus d’octobre 2022 à septembre 2023 (douze mois) dépassent trois fois le smic (soit 62 444,19 €) seront soumis à cotisations sociales, conformément à la loi.

## Non substitution au salaire

Cette prime ne se substitue pas au salaire. Elle est donc versée en plus du salaire contractuel.

## Date d’application

Octobre 2023.

## Références

- [^1] https://travail-emploi.gouv.fr/droit-du-travail/la-remuneration/article/la-prime-de-partage-de-la-valeur

